﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Base;
using Cyberozunu.OnlineTaxApplication.Entity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer.Interface
{
    public interface ICustomerService : IBaseService<Customer>
    {
        Task<Customer> Register(Guid id);
        Task<Customer> GetOTP(Customer entity, ClaimsPrincipal clientClaims);
    }
}
