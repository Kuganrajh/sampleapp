﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer.Interface
{
    public interface IPaymentGWService
    {
        #region Web View
        Task<object> CreateSession(decimal amount, Guid id, string cancelUrl, string successUrl);
        Task<bool> VerifyCallBack(string session);
        #endregion
    }
}
