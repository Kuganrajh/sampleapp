﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Base;
using Cyberozunu.OnlineTaxApplication.Entity.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer
{
    public interface ISectionService : IBaseService<Section>
    {
    }
}
