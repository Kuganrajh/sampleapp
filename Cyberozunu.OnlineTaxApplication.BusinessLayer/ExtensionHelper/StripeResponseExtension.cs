﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Model.Stripe;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer.ExtensionHelper
{
    public static class StripeResponseExtension
    {
        public static StripeResponseModel CreateStripeResponse(this object response, string publishableKey, string entityId, string url)
        {
            StripeResponseModel stripeResponseModel = new StripeResponseModel()
            {
                PublishableKey = publishableKey,
                Response = response,
                EntityId = entityId,
                Url = url
            };
            return stripeResponseModel;
        }
    }
}
