﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Base;
using Cyberozunu.OnlineTaxApplication.BusinessLayer.Interface;
using Cyberozunu.OnlineTaxApplication.DAL.Base;
using Cyberozunu.OnlineTaxApplication.Entity;
using CyberOzunu.Core.CacheManager;
using CyberOzunu.Core.EFCore.Model;
using CyberOzunu.ExceptionHandler.Exceptions;
using CyberOzunu.Security.Constant;
using CyberOzunu.Security.Models;
using CyberOzunu.Security.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer.Implementation
{
    internal class CustomerService : BaseService<Customer>, ICustomerService
    {
       
        private readonly IUserService _userService;
        private readonly InMemoryCache<Customer> _memoryCache;

        public CustomerService(BaseRepository<Customer> baseRepository, IUserService userService, InMemoryCache<Customer> memoryCache) : base(baseRepository)
        {
            _userService = userService;
            _memoryCache = memoryCache;
        }


        public async Task<Customer> GetOTP(Customer entity, ClaimsPrincipal clientClaims)
        {
            FilterParameter filterParameter = new FilterParameter();
            filterParameter.Filters.Add(new CyberOzunu.LinqFunction.Model.FilterModel() { Operator = CyberOzunu.LinqFunction.Common.Operation.EqualTo, Property = nameof(entity.PhoneNumber), value = entity.PhoneNumber });
            var existingCustomer = await _baseRepository.GetAll(filterParameter);
            if (existingCustomer != null && existingCustomer.Count > 0)
            {
                throw new BadRequestException("Already a customer is registered with the phone number " + entity.PhoneNumber);
            }
            entity.Status = Entity.Common.Enum.CustomerStatus.Active;

            var identityResult = await RegisterCustomer(entity, clientClaims);
            if (identityResult != null)
            {
                entity.Token = identityResult.Token;
                entity.RefreshToken = identityResult.RefreshToken;
                entity.Id = identityResult.UserId;
            }
            _memoryCache.AddOrUpdate("tempcustomer_" + entity.Id.ToString(), entity);
            entity.OTP = identityResult.OTP;
            return entity;

        }

        public async Task<Customer> Register(Guid id)
        {
            FilterParameter filterParameter = new FilterParameter();
            Customer entity = GetCustomerInCache(id);
            var result = await base.Add(entity);
            return result;
        }

        private async Task<TokenReponseModel> RegisterCustomer(Customer entity, ClaimsPrincipal clientClaims)
        {
            RegisterRequestModel registerRequestModel = new RegisterRequestModel()
            {
                Email = entity.Email,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Password = entity.Password,
                UserName = entity.PhoneNumber,
                RoleId = UserRoleId.Customer,
                EnableTwofactor = true
            };
            var result = await _userService.RegisterUser(registerRequestModel, clientClaims);
            return result;
        }

        private Customer GetCustomerInCache(Guid id)
        {
            Customer customer = _memoryCache.Get("tempcustomer_" + id.ToString());
            _memoryCache.Delete("tempcustomer_" + id.ToString());
            return customer;
        }
    }
}
