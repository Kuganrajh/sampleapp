﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Base;
using Cyberozunu.OnlineTaxApplication.BusinessLayer.Interface;
using Cyberozunu.OnlineTaxApplication.DAL.Base;
using Cyberozunu.OnlineTaxApplication.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer.Implementation
{
    internal class PackageService : BaseService<Package>, IPackageService
    {
        public PackageService(BaseRepository<Package> baseRepository):base(baseRepository)
        {

        }
    }
}
