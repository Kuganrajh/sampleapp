﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Base;
using Cyberozunu.OnlineTaxApplication.DAL.Base;
using Cyberozunu.OnlineTaxApplication.Entity.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer.Implementation
{
    internal class SectionService : BaseService<Section>, ISectionService
    {
        public SectionService(BaseRepository<Section> baseRepository) : base(baseRepository)
        {
        }
    }
}
