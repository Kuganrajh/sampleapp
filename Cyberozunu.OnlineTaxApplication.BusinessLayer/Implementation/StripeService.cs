﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.ExtensionHelper;
using Cyberozunu.OnlineTaxApplication.BusinessLayer.Interface;
using Cyberozunu.OnlineTaxApplication.BusinessLayer.Model.Stripe;
using CyberOzunu.ExceptionHandler.Exceptions;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Stripe;
using Stripe.Checkout;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer.Implementation
{
    public class StripeService : IPaymentGWService
    {
        private readonly IOptions<StripeConfigurationKey> _stripeConfigurationKey;
        public StripeService(IOptions<StripeConfigurationKey> stripeConfigurationKey)
        {
            _stripeConfigurationKey = stripeConfigurationKey;        
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="module"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<object> CreateSession(decimal amount,  Guid id, string cancelUrl, string successUrl)
        {
            StripeConfiguration.ApiKey = _stripeConfigurationKey.Value.SecretKey;
            var label = await ValidateGetLabelAsync(id, amount);
            Dictionary<string, string> metaData = new Dictionary<string, string>();
            metaData.Add("Id", id.ToString());

            SessionLineItemOptions sessionLineItemOptions = new SessionLineItemOptions
            {
                Amount = Convert.ToInt32(amount * 100),
                Currency = "gbp",
                Name = label,
                Quantity = 1

            };

            var options = new SessionCreateOptions
            {
                SuccessUrl = !string.IsNullOrEmpty(successUrl) ? successUrl : _stripeConfigurationKey.Value.SuccessUrl,
                CancelUrl = !string.IsNullOrEmpty(cancelUrl) ? cancelUrl : _stripeConfigurationKey.Value.CancelUrl,
                PaymentMethodTypes = new List<string>() { "card", },
                LineItems = new List<SessionLineItemOptions>() { sessionLineItemOptions, },
                Mode = "payment",
                PaymentIntentData = new SessionPaymentIntentDataOptions
                {
                    CaptureMethod = "automatic",
                },
                Locale = "en-GB",
                SubmitType = "pay",
                Metadata = metaData,
            };
            var service = new SessionService();
            var stripeResult = await service.CreateAsync(options);
            return stripeResult.CreateStripeResponse(_stripeConfigurationKey.Value.PublishableKey, _stripeConfigurationKey.Value.EntityId, _stripeConfigurationKey.Value.HtmlURL);
        }

        public async Task<bool> VerifyCallBack(string session)
        {
            try
            {
                StripeConfiguration.ApiKey = _stripeConfigurationKey.Value.SecretKey;

                var service = new SessionService();
                var value = service.Get(session);

                string json = JsonConvert.SerializeObject(value);
                //var json = await new StreamReader(_httpContextAccessor.HttpContext.Request.Body).ReadToEndAsync();
                //var header = _httpContextAccessor.HttpContext.Request.Headers["Stripe-Signature"];
                //var stripeEvent = EventUtility.ConstructEvent(
                //                  json, _httpContextAccessor.HttpContext.Request.Headers["Stripe-Signature"],  _stripeConfigurationKey.Value.SecretKey);
                return true;
            }
            catch (StripeException ex)
            {
                throw ex;
            }

        }

        private async Task<string> ValidateGetLabelAsync(Guid id, decimal amount)
        {
            string label = string.Empty;
          
                label = "Package : Starter";
                if (100 > 100)
                {
                    throw new BadRequestException("You cannot pay less than £ 100" );
                }
            return label;
        }
    }
}
