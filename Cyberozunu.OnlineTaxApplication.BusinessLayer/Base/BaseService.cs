﻿using Cyberozunu.OnlineTaxApplication.DAL.Base;
using Cyberozunu.OnlineTaxApplication.Entity.Common;
using CyberOzunu.Core.EFCore.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer.Base
{
    public class BaseService<T> where T : BaseEntity
    {

        public readonly BaseRepository<T> _baseRepository;

        public BaseService(BaseRepository<T> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async virtual Task<T> Add(T entity)
        {
            var result = await _baseRepository.Add(entity);
            await _baseRepository.SaveChangesAsync();
            return result;
        }

        public async virtual Task<List<T>> AddRange(List<T> entity)
        {
            var result = await _baseRepository.AddRange(entity);
            await _baseRepository.SaveChangesAsync();
            return result;
        }

        public async virtual Task<T> Update(T entity)
        {
            var result = _baseRepository.Update(entity);
            await _baseRepository.SaveChangesAsync();
            return result;
        }

        public async virtual Task<T> Get(Guid id, List<string> includes = null)
        {
            var result = await _baseRepository.Get(id, includes);
            return result;
        }

        public async virtual Task<ResponseStatus> Delete(Guid id)
        {
            var result = await _baseRepository.Delete(id);
            await _baseRepository.SaveChangesAsync();

            return result;
        }

        public async virtual Task<CollectionResult<T>> GetAll(FilterParameter filterParameter)
        {
            var result = await _baseRepository.GetAll(filterParameter);
            return result;
        }

        //public async Task<List<AuditModel>> GetAuditHistory(Guid id)
        //{
        //    List<AuditModel> audits = new List<AuditModel>();

        //    FilterParameter filterParameter = new FilterParameter();
        //    filterParameter.Filters.Add(new LinqFunction.Model.FilterModel() { Operator = LinqFunction.Common.Operation.EqualTo, Property = nameof(AuditHistory.KeyValue), value = id });
        //    var auditHistories = await _baseRepository.GetExternalData<AuditHistory>(filterParameter);

        //    auditHistories = auditHistories.OrderByDescending(p => p.CreatedOn).ToList();

        //    foreach (var history in auditHistories)
        //    {
        //        AuditModel audit = new AuditModel();
        //        audit.ModifiedDate = history.CreatedOn;
        //        audit.User = history.ModifiedBy;
        //        var oldValue = JsonConvert.DeserializeObject<T>(history.OldValues);
        //        var newValue = JsonConvert.DeserializeObject<T>(history.NewValues);

        //        var nJObject = JObject.Parse(history.NewValues);
        //        var oJObject = JObject.Parse(history.OldValues);


        //        FindDiff(nJObject, oJObject);


        //        audit.Changes = newValue.GetAudits(oldValue);
        //        audits.Add(audit);
        //    }

        //    return audits.OrderByDescending(p => p.ModifiedDate).ToList();
        //}


        public JObject FindDiff(JToken Current, JToken Model)
        {
            var diff = new JObject();
            if (JToken.DeepEquals(Current, Model)) return diff;

            switch (Current.Type)
            {
                case JTokenType.Object:
                    {
                        var current = Current as JObject;
                        var model = Model as JObject;
                        var addedKeys = current.Properties().Select(c => c.Name).Except(model.Properties().Select(c => c.Name));
                        var removedKeys = model.Properties().Select(c => c.Name).Except(current.Properties().Select(c => c.Name));
                        var unchangedKeys = current.Properties().Where(c => JToken.DeepEquals(c.Value, Model[c.Name])).Select(c => c.Name);
                        foreach (var k in addedKeys)
                        {
                            diff[k] = new JObject
                            {
                                ["+"] = Current[k]
                            };
                        }
                        foreach (var k in removedKeys)
                        {
                            diff[k] = new JObject
                            {
                                ["-"] = Model[k]
                            };
                        }
                        var potentiallyModifiedKeys = current.Properties().Select(c => c.Name).Except(addedKeys).Except(unchangedKeys);
                        foreach (var k in potentiallyModifiedKeys)
                        {
                            var foundDiff = FindDiff(current[k], model[k]);
                            if (foundDiff.HasValues) diff[k] = foundDiff;
                        }
                    }
                    break;
                case JTokenType.Array:
                    {
                        var current = Current as JArray;
                        var model = Model as JArray;
                        var plus = new JArray(current.Except(model, new JTokenEqualityComparer()));
                        var minus = new JArray(model.Except(current, new JTokenEqualityComparer()));
                        if (plus.HasValues) diff["+"] = plus;
                        if (minus.HasValues) diff["-"] = minus;
                    }
                    break;
                default:
                    diff["+"] = Current;
                    diff["-"] = Model;
                    break;
            }

            return diff;
        }


    }


}
