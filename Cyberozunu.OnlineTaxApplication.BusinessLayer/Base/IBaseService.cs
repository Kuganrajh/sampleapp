﻿using Cyberozunu.OnlineTaxApplication.Entity.Common;
using CyberOzunu.Core.EFCore.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer.Base
{
    public interface IBaseService<T> where T : BaseEntity
    {
        Task<T> Add(T entity);
        Task<List<T>> AddRange(List<T> entity);
        Task<T> Update(T entity);
        Task<T> Get(Guid id, List<string> includes = null);
        Task<ResponseStatus> Delete(Guid id);
        Task<CollectionResult<T>> GetAll(FilterParameter filterParameter);
       // Task<List<AuditModel>> GetAuditHistory(Guid id);

    }
}
