﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Base;
using Cyberozunu.OnlineTaxApplication.BusinessLayer.Implementation;
using Cyberozunu.OnlineTaxApplication.BusinessLayer.Interface;
using Cyberozunu.OnlineTaxApplication.BusinessLayer.Model.Stripe;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer
{
    public static class Extension
    {
        public static void AddBLExtension(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped(typeof(BaseService<>), typeof(BaseService<>));

            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IPaymentGWService, StripeService>();
            services.AddScoped<IPackageService, PackageService>();
            services.AddScoped<ISectionService, SectionService>();
            services.Configure<StripeConfigurationKey>(configuration.GetSection("StripeConfiguration"));

        }
    }
}
