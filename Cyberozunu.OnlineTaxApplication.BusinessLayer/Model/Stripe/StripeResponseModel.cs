﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer.Model.Stripe
{
    public class StripeResponseModel
    {
        public object Response { get; set; }
        public string PublishableKey { get; set; }
        public string EntityId { get; set; }
        public string Url { get; set; }
    }
}
