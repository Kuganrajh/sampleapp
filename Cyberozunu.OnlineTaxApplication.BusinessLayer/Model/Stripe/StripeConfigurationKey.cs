﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.BusinessLayer.Model.Stripe
{
    public class StripeConfigurationKey
    {
        public string SecretKey { get; set; }
        public string PublishableKey { get; set; }
        public string EntityId { get; set; }
        public string HtmlURL { get; set; }
        public string SuccessUrl { get; set; }
        public string CancelUrl { get; set; }
    }
}
