﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Interface;
using CyberOzunu.Core.Controller;
using CyberOzunu.ExceptionHandler.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.WebAPI.Controllers
{
    [Route("api/v1.1/[controller]")]
    [ApiController]
    public class StripeController : CyberOzunuController
    {
        private readonly IPaymentGWService _paymentGWService;
        public StripeController(IPaymentGWService paymentGWService)
        {
            _paymentGWService = paymentGWService;
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost("create-session")]
        public async Task<IActionResult> CreateSession(decimal amount,  Guid id, string cancelUrl, string successUrl)
        {
            try
            {
                var result = await _paymentGWService.CreateSession(amount, id, cancelUrl, successUrl);
                return Ok(result);
            }
            catch (BadRequestException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("session-success/{sessionId}")]
        public async Task<IActionResult> OnSessionSuccess(string sessionId)
        {
            try
            {
                //var isValid = await _paymentGWService.VerifyCallBack(sessionId);          
                return Ok("OK");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("session-cancelled/{sessionId}")]
        public async Task<IActionResult> OnSessionCancelled(string sessionId)
        {
            try
            {
                // var isValid = await _paymentGWService.VerifyCallBack(sessionId);          
                return Ok("OK");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


    }
}
