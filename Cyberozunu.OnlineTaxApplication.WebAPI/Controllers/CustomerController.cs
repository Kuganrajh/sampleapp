﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Interface;
using Cyberozunu.OnlineTaxApplication.Entity;
using Cyberozunu.OnlineTaxApplication.WebAPI.Helper;
using CyberOzunu.Security.Config;
using CyberOzunu.Security.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.WebAPI.Controllers
{
    [Route("api/v1.1/[controller]")]
    [ApiController]
    public class CustomerController : BaseController<Customer>
    {
        private readonly IUserService _userService;
        private readonly ICustomerService _customerService;


        public CustomerController(ICustomerService customerService, IUserService userService) : base(customerService)
        {
            _userService = userService;
            _customerService = customerService;
        }

        public async override Task<IActionResult> Add([FromBody] Customer entity)
        {
            try
            {
                var result = await _customerService.GetOTP(entity, User);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        [Authorize]
        [HttpGet("self-info")]
        public async Task<IActionResult> GetCustomer()
        {
            try
            {
                var tokenType = User.FindFirst(x => x.Type == DefinedClaims.TokenType).Value;
                var header = Request.Headers.Keys.ToList();
                if (tokenType != TokenType.LoginToken.ToString())
                {
                    return BadRequest("Invalid Token Type");
                }
                var userType = User.FindFirst(x => x.Type == DefinedClaims.UserType).Value;
                if (userType != UserType.Customer.ToString())
                {
                    return Forbidden();
                }
                var id = Guid.Parse(User.Identity.Name);
                return await base.Get(id);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

        }
    }
}
