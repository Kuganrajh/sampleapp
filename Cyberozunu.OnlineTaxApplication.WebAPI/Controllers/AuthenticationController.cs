﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Interface;
using CyberOzunu.Core.Controller;
using CyberOzunu.Security.Config;
using CyberOzunu.Security.Models;
using CyberOzunu.Security.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.WebAPI.Controllers
{
    [Route("api/v1.1/[controller]")]
    [ApiController]
    public class AuthenticationController : CyberOzunuController
    {

        private readonly IClientService _clientService;
        private readonly IUserService _userService;
        private readonly IAdminService _adminService;
        private readonly ICustomerService _customerService;

        public AuthenticationController(IClientService clientService, IUserService userService, IAdminService adminService, ICustomerService customerService)
        {
            _clientService = clientService;
            _userService = userService;
            _adminService = adminService;
            _customerService = customerService;
        }

        [HttpGet("Client-token")]
        public async Task<IActionResult> GetClientToken(Guid id, string key)
        {
            try
            {
                var result = await _clientService.GetClientToken(id, key).ConfigureAwait(true);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return InternalServerError(ex);
            }
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost("Self-register")]
        public async Task<IActionResult> SelfRegister(RegisterRequestModel registerRequestModel)
        {
            try
            {
                var result = await _userService.RegisterUser(registerRequestModel, User).ConfigureAwait(true);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost("2FA-authentication")]
        public async Task<IActionResult> TwoFactorValidation(TwoFactorValidationModel twoFactorValidationModel)
        {
            try
            {
                var result = await _userService.TwoFactorValidation(twoFactorValidationModel).ConfigureAwait(true);
                if (result.IsNewUser && result.UserType == UserType.Customer)
                {
                    await _customerService.Register(result.UserId);
                }              
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginRequestModel loginRequest)
        {
            try
            {
                var result = await _userService.Login(loginRequest, User).ConfigureAwait(true);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost("refresh-token")]
        public async Task<IActionResult> RefreshToken(RefreshTokenRequestModel refreshTokenRequestModel)
        {
            try
            {
                var result = await _userService.RefreshToken(refreshTokenRequestModel);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost("add-admin-user")]
        public async Task<IActionResult> AdminUserCreate(RegisterRequestModel registerRequest)
        {
            try
            {
                var result = await _adminService.AddAdminUser(registerRequest, User).ConfigureAwait(true);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        #region Forgot password
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("request-forgot-password")]
        public async Task<IActionResult> ForgotPassword(string userName)
        {
            try
            {
                var result = await _userService.ForgotPassword(userName, User);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost("validate-forgot-password")]
        public async Task<IActionResult> ForgotPassword(string otp, string token)
        {
            try
            {
                var result = await _userService.ForgotPassword(otp, token);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost("change-password")]
        public async Task<IActionResult> ChangePassword(ForgotPasswordRequestModel forgotPasswordRequestModel)
        {
            try
            {
                var result = await _userService.ForgotPassword(forgotPasswordRequestModel);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        #endregion Forgot password

        //#region FCM Token
        //[Authorize]
        //[HttpPost("fcm/attach-token")]
        //public async Task<IActionResult> AttachFCMToken(string token)
        //{
        //    try
        //    {
        //        var result = await _fcmTokenService.AttachToken(token, User);
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return InternalServerError(ex);
        //    }
        //}

        //[Authorize]
        //[HttpDelete("fcm/detach-token")]
        //public async Task<IActionResult> DetachFCMToken()
        //{
        //    try
        //    {
        //        var result = await _fcmTokenService.DetachToken(User);
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return InternalServerError(ex);
        //    }
        //}

        //#endregion FCM Token


    }
}
