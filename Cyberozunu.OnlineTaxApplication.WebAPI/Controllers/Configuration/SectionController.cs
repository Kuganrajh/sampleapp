﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Base;
using Cyberozunu.OnlineTaxApplication.BusinessLayer;
using Cyberozunu.OnlineTaxApplication.Entity.Configuration;
using Cyberozunu.OnlineTaxApplication.WebAPI.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.WebAPI.Controllers.Configuration
{
    [Route("api/[controller]")]
    [ApiController]
    public class SectionController : BaseController<Section>
    {
        public SectionController(ISectionService baseService) : base(baseService)
        {
        }
    }
}
