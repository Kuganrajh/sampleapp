﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Base;
using Cyberozunu.OnlineTaxApplication.BusinessLayer.Interface;
using Cyberozunu.OnlineTaxApplication.Entity;
using Cyberozunu.OnlineTaxApplication.WebAPI.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.WebAPI.Controllers
{
    [Route("api/v1.1/[controller]")]
    [ApiController]
    public class PackageController : BaseController<Package>
    {
        public PackageController(IPackageService baseService) : base(baseService)
        {
        }
    }
}
