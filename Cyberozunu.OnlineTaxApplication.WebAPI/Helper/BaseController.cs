﻿using Cyberozunu.OnlineTaxApplication.BusinessLayer.Base;
using Cyberozunu.OnlineTaxApplication.Entity.Common;
using CyberOzunu.Core.Controller.Model;
using CyberOzunu.Core.EFCore.Model;
using CyberOzunu.ExceptionHandler.Exceptions;
using CyberOzunu.LinqFunction.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.WebAPI.Helper
{
    public class BaseController<T> : ControllerBase where T : BaseEntity
    {
        private readonly IBaseService<T> _baseService;
        protected readonly QuaryParamFunctions<T> _queryParamFunctions;

        public BaseController(IBaseService<T> baseService)
        {
            _baseService = baseService;
            _queryParamFunctions = new QuaryParamFunctions<T>();
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost]
        public virtual async Task<IActionResult> Add([FromBody] T entity)
        {
            try
            {
                var result = await _baseService.Add(entity);
                return Ok(result);
            }
            catch (BadRequestException ex)
            {
                return BadRequest(ex);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPut]
        public virtual async Task<IActionResult> Update([FromBody] T entity)
        {
            try
            {
                var result = await _baseService.Update(entity);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpDelete("{id}")]
        public virtual async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var result = await _baseService.Delete(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet("{id}")]
        public virtual async Task<IActionResult> Get(Guid id)
        {
            try
            {
                var result = await _baseService.Get(id, GetIncludes<T>());
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        public virtual async Task<IActionResult> Get(int? pageNumber, int? pageSize, string searchText, string sortBy, bool isDescending)
        {
            try
            {
                var filters = _queryParamFunctions.GetFilters(Request);
                sortBy = string.IsNullOrEmpty(sortBy) ? "CreatedOn" : sortBy;
                //Assign Values to be filtered
                FilterParameter filterParameter = new FilterParameter()
                {
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                    SortBy = sortBy,
                    IsDecending = isDescending,
                    Filters = filters,
                    SearchText = searchText
                };
                var result = await _baseService.GetAll(filterParameter);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        //[HttpGet("Audit/{id}")]
        //public async Task<IActionResult> GetAudit(Guid id)
        //{
        //    try
        //    {
        //        var result = await _baseService.GetAuditHistory(id).ConfigureAwait(true);
        //        return Ok(result);

        //    }
        //    catch (Exception ex)
        //    {
        //        return InternalServerError(ex);
        //    }
        //}

        #region IActionResult Handler

        protected IActionResult ModelStateError(List<ModelError> errors)
        {
            Results results = new Results();
            Error error = new Error();
            results.IsError = true;
            var errorItem = errors.FirstOrDefault();
            error.Title = errorItem.GetType().Name;
            error.Status = "400";
            error.Details = errorItem.ErrorMessage;
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.BadRequest);
        }
        protected IActionResult UnAuthorized(string message)
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Unauthorized";
            error.Status = "401";
            error.Details = message;
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.Unauthorized);
        }
        protected IActionResult NotFound(string message)
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Not found";
            error.Status = "404";
            error.Details = message;
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.NotFound);
        }
        protected IActionResult Forbidden()
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Forbidden";
            error.Details = "You don't have permission to access the API";
            error.Status = "403";
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.Forbidden);
        }
        protected IActionResult InternalServerError(Exception ex)
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Internal Server Error";
            error.Status = "500";
            error.Details = ex.Message;
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.InternalServerError);
            //  throw new InternalServerErrorException( ex.Message, ex );
        }
        protected IActionResult InternalServerError(string message)
        {
            Exception ex = new Exception(message);
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Internal Server Error";
            error.Status = "500";
            error.Details = ex.Message;
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.InternalServerError);
            //throw new InternalServerErrorException( message );
        }
        protected new IActionResult NoContent()
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "No Content";
            error.Status = "204";
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.NoContent);
        }
        protected IActionResult BadRequest(Exception ex)
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Bad Request";
            error.Details = ex.Message;
            error.Status = "400";
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.BadRequest);
            // throw new CommonBadRequestException(message);
        }
        protected IActionResult BadRequest(string ex)
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Bad Request";
            error.Details = ex;
            error.Status = "400";
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.BadRequest);
            // throw new CommonBadRequestException(message);
        }
        protected IActionResult UnAuthorized(object entity, bool cacheData = false)
        {
            if (cacheData)
            {
                Type type = entity.GetType();
                var result = type.GetProperty("Value").GetValue(entity);
                return new ObjectResult(result);
            }
            entity = entity == null ? new List<object>() : entity;
            Results results = new Results();
            results.Result = entity;
            return results.AsActionResult((int)HttpStatusCode.Unauthorized);
        }
        protected IActionResult Ok(object entity, bool cacheData = false)
        {
            if (cacheData)
            {
                Type type = entity.GetType();
                var result = type.GetProperty("Value").GetValue(entity);
                return new ObjectResult(result);
            }
            entity = entity == null ? new List<object>() : entity;
            Results results = new Results();
            results.Result = entity;
            return results.AsActionResult((int)HttpStatusCode.OK);
        }
        protected List<string> GetIncludes<TEntity>() where TEntity : BaseEntity
        {
            var requestedIncludes = Request.Query.Where(p => p.Key.ToLower().Contains("include"));
            var include = requestedIncludes.FirstOrDefault();
            if (include.Key != null)
            {
                var includeList = include.Value.ToString().Split(",").ToList();
                BaseEntity.ValidatePropertyExistenceForInclude<TEntity>(includeList);

                includeList.ForEach(p =>
                {
                    if (string.IsNullOrEmpty(p))
                    {
                        throw new InvalidIncludeException(p, typeof(TEntity));
                    }
                });
                return includeList;
            }
            else
            {
                return null;
            }
        }

        #endregion  IActionResult Handler
    }

}
