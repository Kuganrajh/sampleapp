﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.Entity.Common.Enum
{
    public enum CustomerStatus
    {
        Active = 1,
        InActive,
        Banned
    }
}
