﻿using CyberOzunu.ExceptionHandler.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.Entity.Common
{
    [Serializable]
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public BaseEntity()
        {
            if (this.Id == Guid.Empty)
            {
                this.CreatedOn = DateTime.UtcNow;
            }
            this.ModifiedOn = DateTime.UtcNow;
        }

        public static void ValidatePropertyExistence<T>(string property) where T : BaseEntity
        {
            if (property != null && typeof(T).GetProperty(property, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance) == null)
            {
                throw new BadRequestException($"The property {property} doesn't exists in the {typeof(T).Name}");
            }
        }

        public static void ValidatePropertyExistenceForInclude<T>(List<string> property) where T : BaseEntity
        {
            T obj = (T)Activator.CreateInstance(typeof(T));
            var includes = obj.AvailableIncludes();
            property.ForEach(p =>
            {
                if (property != null && typeof(T).GetProperty(p, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance) == null && !includes.Contains(p))
                {
                    throw new InvalidIncludeException(p, typeof(T));
                }
            });
        }

        public virtual List<string> AvailableIncludes()
        {
            return new List<string>();
        }

        public BaseEntity DeepClone()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;

                return (BaseEntity)formatter.Deserialize(stream);
            }
        }

        public List<string> GetAudits(BaseEntity oldObject)
        {
            List<string> audits = new List<string>();

            PropertyInfo[] properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                var newValue = property.GetValue(this);
                var oldValue = property.GetValue(oldObject);
                if (newValue != oldValue)
                {
                    StringBuilder stringBuilder = new StringBuilder();

                    stringBuilder.AppendFormat("{0} was changed from {1} to {2}", property.Name, oldValue == null ? "" : oldValue.ToString(), newValue == null ? "" : newValue.ToString());
                    audits.Add(stringBuilder.ToString());
                }
            }
            return audits;
        }

    }
}
