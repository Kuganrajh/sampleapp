﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.Entity.Common
{
    public class ResponseStatus
    {
        public bool Status { get; set; }
        public object Data { get; set; }
        public ResponseStatus()
        {
            this.Status = true;
        }
    }
}
