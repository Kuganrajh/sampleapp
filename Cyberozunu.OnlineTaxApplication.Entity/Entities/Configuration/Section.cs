﻿using Cyberozunu.OnlineTaxApplication.Entity.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.Entity.Configuration
{
    public class Section : BaseEntity
    {
     
        public string Title { get; set; }
        public Guid PackageId { get; set; }
        public bool AllowMultiple { get; set; }
        public Package Package { get; set; }
        public virtual ICollection<SubSection> SubSections { get; set; }
        public virtual ICollection<Field> Fields { get; set; }
    }
}
