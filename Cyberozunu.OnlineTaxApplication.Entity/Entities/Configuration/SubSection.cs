﻿using Cyberozunu.OnlineTaxApplication.Entity.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.Entity.Configuration
{
    public class SubSection : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public bool Required { get; set; }
        public Guid SectionId { get; set; }
        public  virtual ICollection<Field> Fields { get; set; }
        public Section Section { get; set; }
    }
}
