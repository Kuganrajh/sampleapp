﻿using Cyberozunu.OnlineTaxApplication.Entity.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.Entity.Configuration
{
    public class Field : BaseEntity
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public bool IsMandatory { get; set; }
        public bool Note { get; set; }
        public bool? AllowMultiple { get; set; }
        public Guid? SubSectionId { get; set; }
        public Guid? SectionId { get; set; }
        public Section Section { get; set; }
        public SubSection SubSection { get; set; }
        public virtual ICollection<FieldOptions> Options { get; set; }
    }
}
