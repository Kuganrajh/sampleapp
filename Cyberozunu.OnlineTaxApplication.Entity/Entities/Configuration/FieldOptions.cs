﻿using Cyberozunu.OnlineTaxApplication.Entity.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.Entity.Configuration
{
    public class FieldOptions : BaseEntity
    {
        public string Value { get; set; }
        public int MinimumSelection { get; set; }
        public Guid FieldId { get; set; }
        public Field Field { get; set; }
    }
}
