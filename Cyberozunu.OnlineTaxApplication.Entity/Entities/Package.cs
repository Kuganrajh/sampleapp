﻿using Cyberozunu.OnlineTaxApplication.Entity.Common;
using Cyberozunu.OnlineTaxApplication.Entity.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.Entity
{
    public class Package : BaseEntity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Section Section { get; set; }
        public string Tags { get; set; }
    }
}
