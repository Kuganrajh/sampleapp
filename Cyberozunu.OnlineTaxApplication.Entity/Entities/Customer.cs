﻿using Cyberozunu.OnlineTaxApplication.Entity.Common;
using Cyberozunu.OnlineTaxApplication.Entity.Common.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.Entity
{
    public class Customer : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string NI_Number { get; set; }
        public string UTR_Number { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public CustomerStatus Status { get; set; }

        [NotMapped]
        public string Password { get; set; }
        [NotMapped]
        public string Token { get; set; }
        [NotMapped]
        public string RefreshToken { get; set; }
        [NotMapped]
        public string OTP { get; set; }

    }

}
