﻿using Cyberozunu.OnlineTaxApplication.Entity.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;

namespace CyberOzunu.LinqFunction.Generics
{
    public class AddInclude
    {
        public IQueryable Include<T>(IQueryable query, string variable, List<string> includes = null, string fields = null, string addtionalField = "") where T : BaseEntity
        {
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.Append("new (");
            queryBuilder.Append(addtionalField);
            if (fields != null)
            {
                queryBuilder.Append(fields);
            }
            else
            {
                foreach (PropertyInfo property in typeof(T).GetProperties())
                {
                    if (!property.PropertyType.IsSubclassOf(typeof(BaseEntity)) && !IsGenericList(property.PropertyType))
                    {
                        queryBuilder.Append(variable + "." + property.Name + ",");
                    }
                }
            }
            if (includes == null)
            {
                string basicQuery = queryBuilder.ToString();
                queryBuilder = new StringBuilder();
                basicQuery = basicQuery.Remove(basicQuery.Length - 1);
                queryBuilder.Append(basicQuery);
            }
            else
            {
                foreach (var include in includes)
                {
                    if (includes.IndexOf(include) != 0)
                    {
                        queryBuilder.Append(",");
                    }
                    queryBuilder.Append(variable + "." + include);
                }
            }
            queryBuilder.Append(")");
            string selectStateMent = queryBuilder.ToString();
            query = query.Select<T>(selectStateMent);
            return query;
        }


        public bool IsGenericList(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }
            foreach (Type @interface in type.GetInterfaces())
            {
                if (@interface.IsGenericType)
                {
                    if (@interface.GetGenericTypeDefinition() == typeof(ICollection<>))
                    {
                        // if needed, you can also return the type used as generic argument
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
