﻿using System;
using System.Linq.Expressions;

namespace CyberOzunu.LinqFunction.Helpers
{
    public class ExpressionBuilder
    {
        public Expression<Func<T, object>> GetPropertySelector<T>(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new InvalidCastException("The sortby cannot be null");
            }
            var arg = Expression.Parameter(typeof(T), "x");
            var property = Expression.Property(arg, propertyName);
            var conv = Expression.Convert(property, typeof(object));
            var exp = Expression.Lambda<Func<T, object>>(conv, new ParameterExpression[] { arg });
            return exp;
        }
    }
}
