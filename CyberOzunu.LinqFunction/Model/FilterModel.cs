﻿using CyberOzunu.LinqFunction.Common;

namespace CyberOzunu.LinqFunction.Model
{
    public class FilterModel
    {
        public string Property { get; set; }
        public Operation Operator { get; set; }
        public object value { get; set; }
    }
}
