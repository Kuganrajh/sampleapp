﻿using System.Linq.Expressions;

namespace CyberOzunu.LinqFunction.Interfaces
{
    internal interface IBuilderHelper
    {
        Expression GetMemberExpression(Expression param, string propertyName);
    }
}
