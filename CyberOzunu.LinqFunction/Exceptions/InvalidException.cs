﻿using System;

namespace CyberOzunu.LinqFunction.Exceptions
{
    public class InvalidException : Exception
    {
        public InvalidException()
        { }

        public InvalidException(string message)
            : base(message)
        { }

        public InvalidException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }

}
