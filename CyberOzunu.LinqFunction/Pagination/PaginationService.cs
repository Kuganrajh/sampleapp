﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CyberOzunu.LinqFunction.Pagination
{
    public static class PaginationService
    {
        /// <summary>
        /// Gets the pagination.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query.</param>
        /// <param name="page">The page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="isDescending">if set to <c>true</c> [is descending].</param>
        /// <returns></returns>
        public static async Task<PaginationModel<T>> Paginate<T>(this IQueryable<T> query, int? page, int? pageSize, string orderBy, bool isDescending = false)
        {
            Expression<Func<T, object>> expression = GetPropertySelector<T>(orderBy);
            IQueryable<T> orderedQuery = isDescending ?
                query.OrderByDescending(expression) :
                query.OrderBy(expression);

            if (page.HasValue && pageSize.HasValue)
                orderedQuery = orderedQuery
                    .Skip((page.Value - 1) * pageSize.Value)
                    .Take(pageSize.Value);

            return new PaginationModel<T>
            {
                TotalItems = await Task.Run(() => query.Count()),
                PageSize = pageSize,
                Page = page,
                Results =  orderedQuery
            };
        }

        /// <summary>
        /// Converts the specified pagination to another type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="pagination">The pagination.</param>
        /// <param name="viewModels">The view models.</param>
        /// <returns></returns>
        public static PaginationModel<TResult> ToPaginatedViewModel<T, TResult>(this PaginationModel<T> pagination, IQueryable<TResult> viewModels)
        {
            return new PaginationModel<TResult>
            {
                TotalItems = pagination.TotalItems,
                Page = pagination.Page,
                PageSize = pagination.PageSize,
                Results = viewModels
            };
        }

        private static Expression<Func<T, object>> GetPropertySelector<T>(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new InvalidCastException("The sortby cannot be null");
            }
            var arg = Expression.Parameter(typeof(T), "x");
            var property = Expression.Property(arg, propertyName);
            var conv = Expression.Convert(property, typeof(object));
            var exp = Expression.Lambda<Func<T, object>>(conv, new ParameterExpression[] { arg });
            return exp;
        }
    }
}
