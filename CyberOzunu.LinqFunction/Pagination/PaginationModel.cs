﻿using System.Collections.Generic;
using System.Linq;

namespace CyberOzunu.LinqFunction.Pagination
{
    public class PaginationModel<T>
    {
        public int? Page { get; set; }

        public int? PageSize { get; set; }

        public int TotalItems { get; set; }

        public IQueryable<T> Results { get; set; }
    }

}
