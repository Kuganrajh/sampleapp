﻿using System;

namespace CyberOzunu.LinqFunction.Common
{
    public class FilterExpection : Exception
    {
        public FilterExpection()
        { }

        public FilterExpection(string message)
            : base(message)
        { }

        public FilterExpection(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
