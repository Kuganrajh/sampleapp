﻿namespace CyberOzunu.LinqFunction.Common
{
    public class OperationConverter
    {
        public Operation ConvertToOperator(string value)
        {
            switch (value)
            {
                case "eq":
                    return Operation.EqualTo;
                case "neq":
                    return Operation.NotEqualTo;
                case "like":
                    return Operation.Contains;
                case "greaterorequal":
                    return Operation.GreaterThanOrEqualTo;
                default:
                    return Operation.EqualTo;
            }
        }
    }
}
