﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CyberOzunu.LinqFunction.Model;

namespace CyberOzunu.LinqFunction.Common
{
    public class QuaryParamFunctions<T>
    {
        public List<FilterModel> GetFilters(HttpRequest Request)
        {
            List<FilterModel> filters = new List<FilterModel>();
            var requestedFilter = Request.Query.Where(p => p.Key.ToLower().Contains("filter"));
            OperationConverter operationConverter = new OperationConverter();
            if (requestedFilter.Any())
            {
                requestedFilter.ToList().ForEach(f =>
              {
                  FilterModel filterModel = new FilterModel();
                  var key = f.Key.ToLower().Replace("filter", "").Replace("[", "").Replace("]", "");
                  var arr = key.Split(":");
                  if (arr.Length != 2)
                  {
                      throw new FilterExpection($"The way the filter is used is incorrect. The format should be `[Filter]fieldname:operator=value` ");
                  }
                  Type type = ValidatePropertyExistenceForFilter<T>(arr[0]);

                  filterModel.Property = arr[0];
                  filterModel.Operator = operationConverter.ConvertToOperator(arr[1]);
                  try
                  {
                      if (type.BaseType == typeof(Enum))
                      {
                          string fullName = type.FullName;
                          int value = Convert.ToInt32(f.Value.ToString());
                          filterModel.value = Enum.ToObject(type, value);
                      }
                      else
                      {
                          filterModel.value = type == typeof(System.Guid) ? Guid.Parse(f.Value.ToString()) : Convert.ChangeType(f.Value.ToString(), type);
                      }
                  }
                  catch(Exception ex)
                  {
                      throw new FilterExpection($"The value '{f.Value.ToString()  }'is not a valid value for the field {arr[0]}");
                  }
                  filters.Add(filterModel);
              });
            }
            return filters;
        }

        //public object GetValue(Type type)
        //{
        //    object value = null;
        //    if(type == typeof(System.Guid))
        //    {
        //        value = 
        //    }
        //}
        public static Type ValidatePropertyExistenceForFilter<P>(string property)
        {
            if (property != null && typeof(P).GetProperty(property, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance) == null)
            {
                throw new FilterExpection($"The field  '{property}'is not a valid filter for {typeof(P).Name}");
            }

            //Type type = typeof( P ).GetProperty( property, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance).GetType();
            PropertyInfo prop = typeof(P).GetProperty(property, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            Type type = prop.PropertyType;
            return type;
        }

        //public static void ValidatePropertyExistenceForFilter<T> ( string property )
        //{
        //    if( property != null && typeof( T ).GetProperty( property , BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance ) == null )
        //    {
        //        throw new Exception( $"The filter '{property}' doesn't exists for {typeof( T ).Name}" );
        //    }
        //}

    }

    public class Enum<EnumType> where EnumType : struct, IConvertible
    {

        /// <summary>
        /// Retrieves an array of the values of the constants in a specified enumeration.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static EnumType[] GetValues()
        {
            return (EnumType[])Enum.GetValues(typeof(EnumType));
        }

        /// <summary>
        /// Converts the string representation of the name or numeric value of one or more enumerated constants to an equivalent enumerated object.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static EnumType Parse(string name)
        {
            return (EnumType)Enum.Parse(typeof(EnumType), name);
        }

        /// <summary>
        /// Converts the string representation of the name or numeric value of one or more enumerated constants to an equivalent enumerated object.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="ignoreCase"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static EnumType Parse(string name, bool ignoreCase)
        {
            return (EnumType)Enum.Parse(typeof(EnumType), name, ignoreCase);
        }

        /// <summary>
        /// Converts the specified object with an integer value to an enumeration member.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static EnumType ToObject(object value)
        {
            return (EnumType)Enum.ToObject(typeof(EnumType), value);
        }
    }
}
