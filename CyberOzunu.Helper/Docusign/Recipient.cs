﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core.Docusign
{
    public class Recipient
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public IFormFile File { get; set; }
    }
}
