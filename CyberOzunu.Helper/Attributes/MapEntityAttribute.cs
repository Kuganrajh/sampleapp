﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MapEntityAttribute : Attribute
    {
        public string Name { get; set; }
        public Type Type { get; set; }

        public MapEntityAttribute(string name, Type type = null)
        {
            this.Name = name;
            this.Type = type;
        }
    }
}
