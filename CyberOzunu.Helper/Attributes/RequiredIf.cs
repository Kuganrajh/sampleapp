﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Text;

namespace CyberOzunu.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class RequiredIfAttribute : ValidationAttribute
    {
        private IHttpContextAccessor _httpContextAccessor;

        private String PropertyName { get; set; }
        private new String ErrorMessage { get; set; }
        private Object DesiredValue { get; set; }
        private HttpMethod HttpMethod { get; set; }

        public RequiredIfAttribute(String propertyName, Object desiredvalue, String errormessage)
        {
            this.PropertyName = propertyName;
            this.DesiredValue = desiredvalue;
            this.ErrorMessage = errormessage;
        }
        public RequiredIfAttribute(string httpMethod, string errormessage)
        {
            this.HttpMethod = new HttpMethod(httpMethod);
            this.ErrorMessage = errormessage;
            _httpContextAccessor = new HttpContextAccessor();
        }




        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            Object instance = context.ObjectInstance;
            Type type = instance.GetType();
            if (this.DesiredValue != null)
            {
                Object propertyvalue = type.GetProperty(PropertyName).GetValue(instance, null);
                if (propertyvalue.ToString() == DesiredValue.ToString() && (value == null || (value.GetType() == typeof(String) && string.IsNullOrEmpty(value.ToString()))))
                {
                    return new ValidationResult(ErrorMessage);
                }
            }
            else if (this.HttpMethod != null)
            {
                var method = new HttpMethod(_httpContextAccessor.HttpContext.Request.Method);
                if (method == this.HttpMethod)
                {

                    if (value == null || (value.GetType() == typeof(String) && string.IsNullOrEmpty(value.ToString())) || (value.GetType() == typeof(Guid) && (Guid)value == Guid.Empty))
                    {
                        return new ValidationResult(ErrorMessage);
                    }
                }
            }




            //foreach( var item in this.DesiredValue )
            //{
            //    if( propertyvalue.ToString( ) == item.ToString( ) && value == null )
            //    {
            //        return new ValidationResult( ErrorMessage );
            //    }
            //}
            return ValidationResult.Success;
        }
    }
}
