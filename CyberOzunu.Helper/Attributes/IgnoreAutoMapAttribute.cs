﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreAutoMapAttribute : Attribute
    {
    }
}
