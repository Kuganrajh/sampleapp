﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core.Security
{
    public interface ISymmetricEncryption
    {
        string Encrypt(string plainText);
        string Decrypt(string cipherText);
        bool verify(string text, string chiperText);
    }
}
