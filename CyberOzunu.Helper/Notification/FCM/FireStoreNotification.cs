﻿using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Core.Notification.FCM
{
    internal class FireStoreNotification : IFireStoreNotification
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public FireStoreNotification(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task<bool> SendNotification(List<FCMMessageModel> data)
        {
            try
            {
                var path = _webHostEnvironment.ContentRootPath + "\\Keys\\FirebaseAuth.json";
                FirebaseApp app = null;
                try
                {
                    app = FirebaseApp.Create(new AppOptions()
                    {
                        Credential = GoogleCredential.FromFile(path)
                    }, "Erica");
                }
                catch (Exception ex)
                {
                    app = FirebaseApp.GetInstance("Erica");
                }
                var fcm = FirebaseMessaging.GetMessaging(app);

                List<Message> messages = new List<Message>();

                foreach (var item in data)
                {
                    Message msg = new Message()
                    {
                        Notification = new FirebaseAdmin.Messaging.Notification
                        {
                            Title = item.Title,
                            Body = item.Body,
                            ImageUrl = item.ImageURL
                        },
                        Data = item.Data,
                        Webpush = new WebpushConfig
                        {
                            FcmOptions = new WebpushFcmOptions()
                            {
                                Link = item.Link
                            }
                        },

                        Token = item.Token
                    };
                    messages.Add(msg);
                }

                var result = await fcm.SendAllAsync(messages);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> SendNotification(FCMMessageModel data)
        {
            try
            {
                var path = _webHostEnvironment.ContentRootPath + "\\Keys\\FirebaseAuth.json";
                FirebaseApp app = null;
                try
                {
                    app = FirebaseApp.Create(new AppOptions()
                    {
                        Credential = GoogleCredential.FromFile(path)
                    }, "Erica");
                }
                catch (Exception ex)
                {
                    app = FirebaseApp.GetInstance("Erica");
                }
                var fcm = FirebaseMessaging.GetMessaging(app);

                Message msg = new Message()
                {
                    Notification = new FirebaseAdmin.Messaging.Notification
                    {
                        Title = data.Title,
                        Body = data.Body,
                        ImageUrl = data.ImageURL
                    },
                    Data = data.Data,
                    Webpush = new WebpushConfig
                    {
                        FcmOptions = new WebpushFcmOptions()
                        {
                            Link = data.Link
                        }
                    },

                    Token = data.Token
                };

                var result = await fcm.SendAsync(msg);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> MultiCastNotification(FCMMultiCastMessageModel data)
        {
            try
            {
                var path = _webHostEnvironment.ContentRootPath + "\\Keys\\FirebaseAuth.json";
                FirebaseApp app = null;
                try
                {
                    app = FirebaseApp.Create(new AppOptions()
                    {
                        Credential = GoogleCredential.FromFile(path)
                    }, "Erica");
                }
                catch (Exception ex)
                {
                    app = FirebaseApp.GetInstance("Erica");
                }
                var fcm = FirebaseMessaging.GetMessaging(app);


                MulticastMessage msg = new MulticastMessage()
                {
                    Notification = new FirebaseAdmin.Messaging.Notification
                    {
                        Title = data.Title,
                        Body = data.Body,
                        ImageUrl = data.ImageURL,
                    },
                    Data = data.Data,
                    Webpush = new WebpushConfig
                    {
                        FcmOptions = new WebpushFcmOptions()
                        {
                            Link = data.Link
                        }
                    },

                    Tokens = data.Tokens
                };
                var result = await fcm.SendMulticastAsync(msg);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
