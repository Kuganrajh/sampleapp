﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core.Notification.FCM
{
    public class FCMMessageModel
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string ImageURL { get; set; }
        public string Link { get; set; }
        public string Token { get; set; }
        public Dictionary<string, string> Data { get; set; }
    }
}
