﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Core.Notification.FCM
{
    public interface IFireStoreNotification
    {
        Task<bool> SendNotification(List<FCMMessageModel> data);
        Task<bool> SendNotification(FCMMessageModel data);
        Task<bool> MultiCastNotification(FCMMultiCastMessageModel data);
    }
}
