﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core.Notification.Model
{
    public class TextItConfig
    {
        public bool enable { get; set; }
        public string Id { get; set; }
        public string PW { get; set; }
        public string Baseurl { get; set; }
    }
}
