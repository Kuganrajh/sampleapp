﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core.Notification.Model
{
    class TwilioConfig
    {
        public string AccountSID { get; set; }
        public string AuthToken { get; set; }
        public string MessageNumber { get; set; }
        public string MessageId { get; set; }
        public bool Enabled { get; set; }


    }
}
