﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Core.Notification.SMS
{
    public interface ISMSService
    {
        Task Send(string to, string message);
    }
}
