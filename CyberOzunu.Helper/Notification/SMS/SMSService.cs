﻿using CyberOzunu.Core.Notification.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;


namespace CyberOzunu.Core.Notification.SMS
{
    internal class SMSService : ISMSService
    {
        private readonly TextItConfig _textItConfig;
        private readonly TwilioConfig _twilioConfig;

        public SMSService(TextItConfig textItConfig, TwilioConfig twilioConfig)
        {
            _textItConfig = textItConfig;
            _twilioConfig = twilioConfig;
        }

        public async Task Send(string to, string message)
        {
            string extension = to.Substring(0, 3);
            if (extension == "+94" && _textItConfig.enable)
            {             
                    WebClient client = new WebClient();

                    client.QueryString.Add("id", _textItConfig.Id);
                    client.QueryString.Add("pw", _textItConfig.PW);
                    client.QueryString.Add("to", to);
                    client.QueryString.Add("text", message);

                    string baseurl = _textItConfig.Baseurl;
                    Stream data = client.OpenRead(baseurl);
                    StreamReader reader = new StreamReader(data);
                    string s = reader.ReadToEnd();
                    data.Close();
                    reader.Close();
            }
            else if(_twilioConfig.Enabled)
            {
                    string accountSid = _twilioConfig.AccountSID;
                    string authToken = _twilioConfig.AuthToken;

                    TwilioClient.Init(accountSid, authToken);
                    var messageResource = MessageResource.Create(
                    body: message,
                    messagingServiceSid: _twilioConfig.MessageId,
                    to: new Twilio.Types.PhoneNumber(to)
                    );

            }
        }
    }
}
