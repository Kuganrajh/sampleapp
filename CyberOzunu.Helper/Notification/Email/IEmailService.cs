﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Core.Notification.Email
{
    public interface IEmailService
    {
        Task<bool> SendEmail(EmailMessage message);
    }
}
