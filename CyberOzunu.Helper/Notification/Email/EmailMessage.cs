﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CyberOzunu.Core.Notification.Email
{
    public class EmailMessage
    {
        public List<string> To { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string From { get; set; }

        public List<string> CC { get; set; }

        public EmailMessage()
        {
            To = new List<string>();
            CC = new List<string>();
        }

        public EmailMessage(IEnumerable<string> to, string subject, string content, IEnumerable<string> cc = null)
        {
            To = new List<string>();
            CC = new List<string>();

            To.AddRange(to);
            Subject = subject;
            Content = content;

            if (cc != null)
            {
                CC.AddRange(cc);
            }
        }
    }
}
