﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Core.Notification.Email
{
    internal class EmailService : IEmailService
    {
        private readonly EmailConfiguration _emailConfig;

        public EmailService(EmailConfiguration emailConfiguration)
        {
            _emailConfig = emailConfiguration;
        }

        public async Task<bool> SendEmail(EmailMessage message)
        {
            var emailMessage = CreateEmailMessage(message);
            var isSend = await Send(emailMessage);
            return isSend;

        }

        private MimeMessage CreateEmailMessage(EmailMessage message)
        {
            List<MailboxAddress> to = new List<MailboxAddress>();
            List<MailboxAddress> cc = new List<MailboxAddress>();

            foreach (var item in message.To)
            {
                to.Add(new MailboxAddress(item));
            }
            if(message.CC!=null && message.CC.Count > 0)
            {
                foreach (var item in message.CC)
                {
                    cc.Add(new MailboxAddress(item));
                }
            }

            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_emailConfig.From));
            emailMessage.To.AddRange(to);
            emailMessage.Subject = message.Subject;
            if (message.CC != null)
            {
                emailMessage.Cc.AddRange(cc);
            }
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = message.Content };

            return emailMessage;
        }

        private async Task<bool> Send(MimeMessage mailMessage)
        {
            bool isSuccess = false;
            using (var client = new SmtpClient())
            {
                try
                {
                    await client.ConnectAsync(_emailConfig.SmtpServer, _emailConfig.Port, SecureSocketOptions.StartTls);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    await client.AuthenticateAsync(_emailConfig.UserName, _emailConfig.Password);
                    await client.SendAsync(mailMessage);

                    isSuccess = true;

                }
                catch (Exception ex)
                {
                    var message = ex;

                    isSuccess = false;
                    throw ex;

                    //throw new EmailException(_localizationService.GetLocalizedStringValue("MessageSendFail").Result.ToString());
                }
                finally
                {
                    client.Disconnect(true);
                    client.Dispose();

                }
            }
            return isSuccess;
        }
    }
}
