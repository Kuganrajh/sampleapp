﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core.CacheManager
{
    public class InMemoryCache<T>
    {
        private readonly IMemoryCache _memoryCache;
        public InMemoryCache(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public void AddOrUpdate(Guid key, T obj)
        {
            _memoryCache.Set<T>(key, obj);
        }
        public void AddOrUpdate(string key, T obj)
        {
            _memoryCache.Set<T>(key, obj);
        }

        public T Get(Guid key)
        {
            var value = default(T);
            _memoryCache.TryGetValue<T>(key, out value);
            return value;
        }
        public T Get(string key)
        {
            var value = default(T);
            _memoryCache.TryGetValue<T>(key, out value);
            return value;
        }

        public void Delete(Guid key)
        {
            _memoryCache.Remove(key);
        }
        public void Delete(string key)
        {
            _memoryCache.Remove(key);
        }
    }
}
