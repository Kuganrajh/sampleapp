﻿using CyberOzunu.Core.Controller.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CyberOzunu.Core.Validation
{
    public class ModelStateValidation
    {
        public BadRequestObjectResult ValidateModel(ActionContext context)
        {
            var message = string.Join(Environment.NewLine, context.ModelState.Values.SelectMany(v => v.Errors.Select(e => e.ErrorMessage)));
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Bad Request";
            error.Status = "400";
            error.Details = message;
            results.Error = error;
            return new BadRequestObjectResult(results);
        }
    }
}
