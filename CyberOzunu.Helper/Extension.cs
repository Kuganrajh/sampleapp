﻿using CyberOzunu.Core.CacheManager;
using CyberOzunu.Core.Notification.Email;
using CyberOzunu.Core.Notification.FCM;
using CyberOzunu.Core.Notification.Model;
using CyberOzunu.Core.Notification.SMS;
using CyberOzunu.Core.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core
{
    public static class Extensions
    {
        public static void AddCoreExtension(this IServiceCollection services, IConfiguration configuration)
        {
            var textItConfiguration = configuration.GetSection("TextItConfig").Get<TextItConfig>();
            services.AddSingleton(textItConfiguration);

            var twilioConfiguration = configuration.GetSection("TwilioConfig").Get<TwilioConfig>();
            services.AddSingleton(twilioConfiguration);

            var emailConfig = configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>();
            services.AddSingleton(emailConfig);

            services.AddScoped<ISMSService, SMSService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IFireStoreNotification, FireStoreNotification>();
            services.AddScoped<ISymmetricEncryption, SymmetricEncryption>();
            services.AddCaching();

        }

        public static void AddCaching(this IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddScoped(typeof(InMemoryCache<>), typeof(InMemoryCache<>));
        }
    }
}
