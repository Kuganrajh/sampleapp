﻿using CyberOzunu.Core.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CyberOzunu.Core.Mapper
{
    public class AutoMapper<T>
    {
        #region Advanced version of automapper

        public T Map(object valueObject, T updateModel)
        {
            if (valueObject == null)
            {
                return default(T);
            }

            var propertyInfo = updateModel.GetType().GetProperties();

            foreach (PropertyInfo property in propertyInfo)
            {

                var ignoreAutoMapper = property.GetCustomAttribute(typeof(IgnoreAutoMapAttribute), true);
                if (ignoreAutoMapper == null)
                {
                    string targetPropertyName = property.Name;
                    var attribute = property.GetCustomAttribute(typeof(MapEntityAttribute), true);
                    if (attribute != null)
                    {
                        MapEntityAttribute mapEntityAttribute = attribute as MapEntityAttribute;
                        targetPropertyName = mapEntityAttribute.Name;
                    }
                    if (IsGeneric(property))
                    {
                        MapGenericType(property, valueObject, updateModel);
                    }
                    else if (property.PropertyType.IsClass && property.PropertyType.Assembly.FullName == updateModel.GetType().Assembly.FullName)
                    {

                        var type = valueObject.GetType().GetProperty(targetPropertyName);
                        if (type != null)
                        {
                            var value = type.GetValue(valueObject);
                            MapSubClass(property, value, updateModel);
                        }
                    }
                    else
                    {
                        MapFields(updateModel, valueObject, property);
                        // Type type = valueObject.GetType();
                    }
                }

            }
            return updateModel;

        }

        public T Map(object valueObject)
        {
            if (valueObject == null)
            {
                return default(T);
            }
            var obj = Activator.CreateInstance<T>();
            var propertyInfo = obj.GetType().GetProperties();
            foreach (PropertyInfo property in propertyInfo)
            {

                var ignoreAutoMapper = property.GetCustomAttribute(typeof(IgnoreAutoMapAttribute), true);
                if (ignoreAutoMapper == null)
                {
                    string targetPropertyName = property.Name;
                    var attribute = property.GetCustomAttribute(typeof(MapEntityAttribute), true);
                    if (attribute != null)
                    {
                        MapEntityAttribute mapEntityAttribute = attribute as MapEntityAttribute;
                        targetPropertyName = mapEntityAttribute.Name;
                    }
                    if (IsGeneric(property))
                    {
                        MapGenericType(property, valueObject, obj);
                    }
                    else if (property.PropertyType.IsClass && property.PropertyType.Assembly.FullName == obj.GetType().Assembly.FullName)
                    {

                        var type = valueObject.GetType().GetProperty(targetPropertyName);
                        if (type != null)
                        {
                            var value = type.GetValue(valueObject);
                            MapSubClass(property, value, obj);
                        }
                    }
                    else
                    {
                        MapFields(obj, valueObject, property);
                        // Type type = valueObject.GetType();
                    }
                }

            }
            return obj;

        }
        private void MapGenericType(PropertyInfo property, object entity, object model)
        {
            string propertyName = string.Empty;
            MapEntityAttribute mapEntityAttribute = null;
            var entityMapperAttributes = property.GetCustomAttributes(typeof(MapEntityAttribute), true).ToList();
            if (entityMapperAttributes != null && entityMapperAttributes.Count > 0)
            {
                mapEntityAttribute = entityMapperAttributes.FirstOrDefault() as MapEntityAttribute;
                propertyName = mapEntityAttribute.Name;
            }
            else
            {
                propertyName = property.Name;
            }

            //Get the type of the object to be created
            Type genericType = property.PropertyType;
            var arguments = genericType.GenericTypeArguments;
            if (arguments.Length > 0)
            {
                Type type = arguments[0];

                var nameSplitArray = propertyName.Split(".");

                if (nameSplitArray.Count() > 1)
                {
                    var objList = GenerateSimpleList(entity, type, nameSplitArray);
                    property.SetValue(model, objList);

                }
                else
                {
                    var parentProperty = entity.GetType().GetProperty(nameSplitArray[0]);
                    if (parentProperty != null)
                    {
                        var parentPropertyValue = entity.GetType().GetProperty(nameSplitArray[0]).GetValue(entity);
                        if (parentPropertyValue != null)
                        {
                            var objList = GenerateList(parentPropertyValue, property, type, nameSplitArray);
                            property.SetValue(model, objList);
                        }
                    }
                }
            }
        }

        public void MapSubClass(PropertyInfo prop, object valueObject, object targetObject)
        {
            if (valueObject != null)
            {
                var obj = Activator.CreateInstance(prop.PropertyType);
                foreach (PropertyInfo property in obj.GetType().GetProperties())
                {
                    var ignoreAutoMapper = property.GetCustomAttribute(typeof(IgnoreAutoMapAttribute), true);
                    if (ignoreAutoMapper == null)
                    {
                        if (IsGeneric(property))
                        {
                            MapGenericType(property, valueObject, obj);
                        }
                        else if (property.PropertyType.IsClass && property.PropertyType.Assembly.FullName == obj.GetType().Assembly.FullName)
                        {
                            string targetPropertyName = GetPropertyName(property);
                            //property.Name;
                            //var attribute = property.GetCustomAttribute(typeof(MapEntityAttribute), true);
                            //if (attribute != null)
                            //{
                            //    MapEntityAttribute mapEntityAttribute = attribute as MapEntityAttribute;
                            //    targetPropertyName = mapEntityAttribute.Name;
                            //}
                            var type = valueObject.GetType().GetProperty(targetPropertyName);
                            if (type != null)
                            {
                                var value = type.GetValue(valueObject);
                                MapSubClass(property, value, obj);
                            }
                        }
                        else
                        {
                            MapFields(obj, valueObject, property);
                            // Type type = valueObject.GetType();
                        }
                    }

                }
                var properName = GetPropertyName(prop);
                var propType = targetObject.GetType().GetProperty(prop.Name);
                if (propType != null)
                {
                    propType.SetValue(targetObject, obj);
                }
            }
        }

        public void MapSubClass(object valueObject, object targetObject)
        {
            if (valueObject != null)
            {
                foreach (PropertyInfo property in targetObject.GetType().GetProperties())
                {
                    var ignoreAutoMapper = property.GetCustomAttribute(typeof(IgnoreAutoMapAttribute), true);
                    if (ignoreAutoMapper == null)
                    {
                        if (IsGeneric(property))
                        {
                            MapGenericType(property, valueObject, targetObject);
                        }
                        else if (property.PropertyType.IsClass && property.PropertyType.Assembly.FullName == targetObject.GetType().Assembly.FullName)
                        {
                            string targetPropertyName = GetPropertyName(property);
                            //    property.Name;
                            //var attribute = property.GetCustomAttribute(typeof(MapEntityAttribute), true);
                            //if (attribute != null)
                            //{
                            //    MapEntityAttribute mapEntityAttribute = attribute as MapEntityAttribute;
                            //    targetPropertyName = mapEntityAttribute.Name;
                            //}
                            var type = valueObject.GetType().GetProperty(targetPropertyName);
                            if (type != null)
                            {
                                var value = type.GetValue(valueObject);
                                MapSubClass(property, value, targetObject);
                            }
                        }
                        else
                        {
                            MapFields(targetObject, valueObject, property);
                            // Type type = valueObject.GetType();
                        }
                    }

                }

            }
        }

        public void MapFields(object returnModel, object valueObject, PropertyInfo targetPropertyInfo)
        {
            if (valueObject == null)
            {
                return;
            }
            string targetPropertyName = targetPropertyInfo.Name;
            Type valuetype = null;
            var attribute = targetPropertyInfo.GetCustomAttribute(typeof(MapEntityAttribute), true);
            if (attribute != null)
            {
                MapEntityAttribute mapEntityAttribute = attribute as MapEntityAttribute;
                valuetype = mapEntityAttribute.Type;
                targetPropertyName = mapEntityAttribute.Name;
            }
            var valueObjectProperty = valueObject.GetType().GetProperty(targetPropertyName);
            if (valueObjectProperty != null)
            {
                var value = valueObjectProperty.GetValue(valueObject);
                SetValue(valuetype, returnModel, value, targetPropertyInfo);
            }
        }
        private void SetValue(Type valueType, object returnModel, object value, PropertyInfo property)
        {
            if (valueType != null)
            {
                var convertedValue = ConvertValue(value, valueType);
                property.SetValue(returnModel, convertedValue);
            }
            else
            {
                property.SetValue(returnModel, value);
            }
        }

        private object GenerateSimpleList(object entity, Type type, string[] arr)
        {
            IList objList = (IList)Activator.CreateInstance((typeof(List<>).MakeGenericType(type)));
            var parentProperty = entity.GetType().GetProperty(arr[0]);
            if (parentProperty != null)
            {
                var parentPropertyValue = entity.GetType().GetProperty(arr[0]).GetValue(entity);
                if (parentPropertyValue != null)
                {
                    foreach (var item in parentPropertyValue as IEnumerable)
                    {
                        var parentType = item.GetType();
                        var propertyValue = parentType.GetProperty(arr[1]).GetValue(item);
                        objList.Add(propertyValue);

                    }
                }
            }
            return objList != null && objList.Count > 0 ? objList : null;
        }

        private object GenerateList(object parentPropertyValue, PropertyInfo property, Type type, string[] arr)
        {
            IList objList = (IList)Activator.CreateInstance((typeof(List<>).MakeGenericType(type)));

            foreach (var item in parentPropertyValue as IEnumerable)
            {
                var propType = property.PropertyType.GetGenericArguments()[0];
                if (IsSimpleType(propType))
                {
                    objList.Add(item);
                }
                else
                {
                    var childModel = Activator.CreateInstance(propType);
                    MapSubClass(item, childModel);
                    objList.Add(childModel);
                }
            }

            return objList != null && objList.Count > 0 ? objList : null;

        }

        private string GetPropertyName(PropertyInfo prop)
        {
            string propertyName = prop.Name;
            Type valuetype = null;
            var attribute = prop.GetCustomAttribute(typeof(MapEntityAttribute), true);
            if (attribute != null)
            {
                MapEntityAttribute mapEntityAttribute = attribute as MapEntityAttribute;
                valuetype = mapEntityAttribute.Type;
                propertyName = mapEntityAttribute.Name;
            }
            return propertyName;
        }
        private bool IsGeneric(PropertyInfo property)
        {
            var interfaces = property.PropertyType.GetInterfaces();
            if (!IsSimpleType(property.PropertyType) && ((property.PropertyType.BaseType != null && property.PropertyType.BaseType.IsGenericType) || interfaces.Contains(typeof(IList)) || interfaces.Contains(typeof(ICollection)) || interfaces.Contains(typeof(IEnumerable))))
            {
                return true;
            }
            return false;
        }
        private object ConvertValue(object obj, Type type)
        {
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.String:
                    return obj == null ? null : obj.ToString();
                case TypeCode.Boolean:
                    return Convert.ToBoolean(obj);
                case TypeCode.Double:
                    return Convert.ToDouble(obj);
                default:
                    return obj;
            }
        }

        private bool IsSimpleType(Type type)
        {
            if (type == typeof(Guid))
            {
                return true;
            }
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.String:
                    return true;
                case TypeCode.Boolean:
                    return true;
                case TypeCode.Double:
                    return true;
                case TypeCode.Int32:
                    return true;
                case TypeCode.Decimal:
                    return true;
                case TypeCode.Byte:
                    return true;
                case TypeCode.SByte:
                    return true;
                case TypeCode.Char:
                    return true;
                case TypeCode.DateTime:
                    return true;
                case TypeCode.Int16:
                    return true;
                case TypeCode.Int64:
                    return true;
                case TypeCode.Single:
                    return true;
                case TypeCode.UInt16:
                    return true;
                case TypeCode.UInt32:
                    return true;
                case TypeCode.UInt64:
                    return true;
                default:
                    return false;
            }
        }
        #endregion Advanced version of automapper

    }
}
