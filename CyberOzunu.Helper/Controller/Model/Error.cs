﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core.Controller.Model
{
    public class Error
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("detail")]
        public string Details { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
