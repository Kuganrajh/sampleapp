﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core.Controller.Model
{
    public class Results
    {
        [JsonProperty("result")]
        public object Result { get; set; }

        [JsonProperty("isError")]
        public bool IsError { get; set; }

        [JsonProperty("error")]
        public Error Error { get; set; }

        public Results()
        {
            Error = new Error();
            Result = new object();
        }

        public IActionResult AsActionResult(int StatusCode)
        {
            Results results = new Results();
            results.Result = this.Result;
            results.IsError = this.IsError;
            results.Error = this.Error;

            return new ObjectResult(this)
            {
                StatusCode = StatusCode,
                Value = results
            };

        }
        public IActionResult AsActionResult()
        {
            Results results = new Results();
            results.IsError = this.IsError;
            results.Result = this.Result;
            results.Error = this.Error;
            return new ObjectResult(this)
            {
                Value = this
            };

        }

    }

}
