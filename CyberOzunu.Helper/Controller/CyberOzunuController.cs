﻿using CyberOzunu.Core.Controller.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace CyberOzunu.Core.Controller
{
    public class CyberOzunuController : ControllerBase 
    {
       

        #region IActionResult Handler

        protected IActionResult ModelStateError(List<ModelError> errors)
        {
            Results results = new Results();
            Error error = new Error();
            results.IsError = true;
            var errorItem = errors.FirstOrDefault();
            error.Title = errorItem.GetType().Name;
            error.Status = "400";
            error.Details = errorItem.ErrorMessage;
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.BadRequest);
        }
        protected IActionResult UnAuthorized(string message)
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Unauthorized";
            error.Status = "401";
            error.Details = message;
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.Unauthorized);
        }
        protected IActionResult NotFound(string message)
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Not found";
            error.Status = "404";
            error.Details = message;
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.NotFound);
        }
        protected IActionResult Forbidden()
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Forbidden";
            error.Details = "You don't have permission to access the API";
            error.Status = "403";
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.Forbidden);
        }
        protected IActionResult InternalServerError(Exception ex)
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Internal Server Error";
            error.Status = "500";
            error.Details = ex.Message;
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.InternalServerError);
            //  throw new InternalServerErrorException( ex.Message, ex );
        }
        protected IActionResult InternalServerError(string message)
        {
            Exception ex = new Exception(message);
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Internal Server Error";
            error.Status = "500";
            error.Details = ex.Message;
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.InternalServerError);
            //throw new InternalServerErrorException( message );
        }
        protected new IActionResult NoContent()
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "No Content";
            error.Status = "204";
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.NoContent);
        }
        protected IActionResult BadRequest(Exception ex)
        {
            Results results = new Results();
            results.IsError = true;
            Error error = new Error();
            error.Title = "Bad Request";
            error.Details = ex.Message;
            error.Status = "400";
            results.Error = error;
            return results.AsActionResult((int)HttpStatusCode.BadRequest);
            // throw new CommonBadRequestException(message);
        }
        protected IActionResult UnAuthorized(object entity, bool cacheData = false)
        {
            if (cacheData)
            {
                Type type = entity.GetType();
                var result = type.GetProperty("Value").GetValue(entity);
                return new ObjectResult(result);
            }
            entity = entity == null ? new List<object>() : entity;
            Results results = new Results();
            results.Result = entity;
            return results.AsActionResult((int)HttpStatusCode.Unauthorized);
        }
        protected IActionResult Ok(object entity, bool cacheData = false)
        {
            if (cacheData)
            {
                Type type = entity.GetType();
                var result = type.GetProperty("Value").GetValue(entity);
                return new ObjectResult(result);
            }
            entity = entity == null ? new List<object>() : entity;
            Results results = new Results();
            results.Result = entity;
            return results.AsActionResult((int)HttpStatusCode.OK);
        }
   
        #endregion  IActionResult Handler
    }

}
