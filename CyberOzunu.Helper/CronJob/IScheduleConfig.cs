﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Core.CronJob
{
    public interface IScheduleConfig<T>
    {
        string CronExpression { get; set; }
        TimeZoneInfo TimeZoneInfo { get; set; }
    }
}
