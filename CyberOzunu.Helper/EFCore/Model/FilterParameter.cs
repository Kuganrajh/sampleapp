﻿using CyberOzunu.LinqFunction.Common;
using CyberOzunu.LinqFunction.Model;
using System;
using System.Collections.Generic;
using System.Text;


namespace CyberOzunu.Core.EFCore.Model
{
    public class FilterParameter
    {
        public string SearchText { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public string SortBy { get; set; }
        public bool IsDecending { get; set; }
        public List<FilterModel> Filters { get; set; }
        public FilterStatementConnector FilterStatementConnector { get; set; }
        public List<string> Includes { get; set; }

        public FilterParameter()
        {
            this.SearchText = string.Empty;
            this.SortBy = null;
            this.IsDecending = false;
            this.FilterStatementConnector = FilterStatementConnector.And;
            this.Filters = new List<FilterModel>();
        }
    }
}
