﻿using CyberOzunu.Security.Entity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Data
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public ApplicationDbContext()
        {

        }

        public DbSet<UserToken> UserTokens { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Origin> Origins { get; set; }
        public DbSet<OriginUser> OriginUsers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Property(p => p.IsActive).HasDefaultValue(true);
            base.OnModelCreating(modelBuilder);
        }
    }
}
