﻿using CyberOzunu.Security.Models;
using CyberOzunu.Security.Repository.Interfaces;
using CyberOzunu.Security.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Service
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;
        private readonly IClaimService _claimService;
        private readonly ITokenService _tokenService;

        public ClientService(IClientRepository clientRepository, IClaimService claimService, ITokenService tokenService)
        {
            _clientRepository = clientRepository;
            _claimService = claimService;
            _tokenService = tokenService; 
        }

        public async Task<TokenReponseModel> GetClientToken(Guid id, string key)
        {
            try
            {
                var client = await _clientRepository.ClientVerification(id, key).ConfigureAwait(true);
                if (client == null)
                {
                    throw new Exception("Clinet verification is failed");
                }

                var clientClaims = await _claimService.GetClientClaims(client).ConfigureAwait(true);

                var token = await _tokenService.GenerateAccessToken(clientClaims, Config.TokenType.ClientAccessToken).ConfigureAwait(true);

                var result = _tokenService.GetTokenResponse(token, "", Config.TokenType.ClientAccessToken);

                return result;
            }
            catch (Exception ex)
            {

                throw;
            }

            
        }
    }
}
