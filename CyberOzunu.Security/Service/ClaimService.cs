﻿using CyberOzunu.Security.Config;
using CyberOzunu.Security.Entity;
using CyberOzunu.Security.Repository.Interfaces;
using CyberOzunu.Security.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Service
{
    public class ClaimService : IClaimService
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IClientRepository _clientRepository;

        public ClaimService(IRoleRepository roleRepository, IClientRepository clientRepository)
        {
            _roleRepository = roleRepository;
            _clientRepository = clientRepository;
        }

        public async Task<IEnumerable<Claim>> GetQuotationClaims(string phonenumber, string email, string otpCode,string firstName,string lastName, ClaimsPrincipal claim)
        {
            var client = await GetClientByClaim(claim).ConfigureAwait(true);

            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(DefinedClaims.ClientId, client.Id.ToString()));
            claims.Add(new Claim(DefinedClaims.ClientName, client.Name));
            claims.Add(new Claim(DefinedClaims.UserType, client.UserType.ToString()));
            claims.Add(new Claim(DefinedClaims.Otp, otpCode));
            claims.Add(new Claim(DefinedClaims.Username, phonenumber));
            claims.Add(new Claim(DefinedClaims.Email, String.IsNullOrWhiteSpace(email) ? "" : email));
            claims.Add(new Claim(DefinedClaims.TokenType, TokenType.QuotationRequestToken.ToString()));
            claims.Add(new Claim(DefinedClaims.FirstName, firstName));
            claims.Add(new Claim(DefinedClaims.LastName, lastName));
            claims.Add(new Claim(DefinedClaims.PhoneNumber, phonenumber));


            return claims;
        }

        public async Task<IEnumerable<Claim>> GetClientClaims(Client client)
        {
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(DefinedClaims.ClientId, client.Id.ToString()));
            claims.Add(new Claim(DefinedClaims.ClientName, client.Name));
            claims.Add(new Claim(DefinedClaims.UserType, client.UserType.ToString()));
            claims.Add(new Claim(DefinedClaims.TokenType, TokenType.ClientAccessToken.ToString()));

            return claims;
        }

        public async Task<Client> GetClientByClaim(ClaimsPrincipal claims)
        {
            var id = claims.FindFirst(x => x.Type == DefinedClaims.ClientId).Value;
            var name = claims.FindFirst(x => x.Type == DefinedClaims.ClientName).Value;

            var client = await _clientRepository.GetClient(Guid.Parse(id)).ConfigureAwait(true);
            return client;
        }

        public async Task<IEnumerable<Claim>> GetProfileClaims(User user, Client client,  TokenType tokenType, Origin origin)
        {
            try
            {
                List<Claim> claims = new List<Claim>();

                var userRoles = await _roleRepository.GetUserRole(Guid.Parse(user.Id), client).ConfigureAwait(true);

                claims.Add(new Claim(ClaimTypes.Name, user.Id?.ToString() == null ? "" : user.Id.ToString()));
                claims.Add(new Claim(DefinedClaims.UserId, user.Id?.ToString() == null ? "" : user.Id.ToString()));
                claims.Add(new Claim(DefinedClaims.FirstName, user.FirstName?.ToString() == null ? "" : user.FirstName.ToString()));
                claims.Add(new Claim(DefinedClaims.LastName, user.LastName?.ToString() == null ? "" : user.LastName.ToString()));
                claims.Add(new Claim(DefinedClaims.Username, user.UserName?.ToString() == null ? "" : user.UserName.ToString()));
                claims.Add(new Claim(DefinedClaims.TokenType, tokenType.ToString()));
                claims.Add(new Claim(DefinedClaims.ClientId, client.Id.ToString()));
                claims.Add(new Claim(DefinedClaims.ClientName, client.Name));
                claims.Add(new Claim(DefinedClaims.UserType, client.UserType.ToString()));
                claims.Add(new Claim(DefinedClaims.OriginId, origin.Id.ToString()));
                claims.Add(new Claim(DefinedClaims.OriginName, origin.Name));

                //get user roles
                if (userRoles != null)
                {
                    foreach (var role in userRoles)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, role.Name));
                    }
                }

                return claims;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<Claim>> GetOtpClaims(User user, string  otpCode, TokenType tokenType, Client client, Origin origin)
        {
            try
            {
              
                List<Claim> otpClaims = new List<Claim> {

                new Claim(ClaimTypes.Name, user.Id.ToString()),
                new Claim(DefinedClaims.UserId, user.Id.ToString()),
                new Claim(DefinedClaims.Username, user.UserName),
                new Claim(DefinedClaims.ClientId, client.Id.ToString()),
                new Claim(DefinedClaims.ClientName, client.Name),
                new Claim(DefinedClaims.TokenType, tokenType.ToString()),
                new Claim(DefinedClaims.OriginId, origin.Id.ToString()),
                new Claim(DefinedClaims.OriginName, origin.Name),
                new Claim(DefinedClaims.Otp ,otpCode )

            };

                return otpClaims;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
