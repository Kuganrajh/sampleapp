﻿
using CyberOzunu.Core.Notification.SMS;
using CyberOzunu.Core.Security;
using CyberOzunu.Security.Config;
using CyberOzunu.Security.Entity;
using CyberOzunu.Security.Models;
using CyberOzunu.Security.Repository;
using CyberOzunu.Security.Repository.Interfaces;
using CyberOzunu.Security.Service.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly ITokenService _tokenService;
        private readonly IClaimService _claimService;
        private readonly IUserTokenRepository _userTokenRepository;
        private readonly IMemoryCache _memoryCache;
        private readonly PasswordHash _passwordHash;
        private readonly IOriginRepository _originRepository;
        private readonly ISMSService _smsService;
        private readonly ISymmetricEncryption _symmetricEncryption;
        public UserService(IOriginRepository originRepository, PasswordHash passwordHash, IMemoryCache memoryCache, IUserRepository userRepository, IRoleRepository roleRepository, 
            ITokenService tokenService, IClaimService claimService, IUserTokenRepository userTokenRepository,ISMSService smsService,ISymmetricEncryption symmetricEncryption)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _tokenService = tokenService;
            _claimService = claimService;
            _userTokenRepository = userTokenRepository;
            _memoryCache = memoryCache;
            _passwordHash = passwordHash;
            _originRepository = originRepository;
            _smsService = smsService;
            _symmetricEncryption = symmetricEncryption;
        }


        public async Task<TokenReponseModel> RegisterUser(RegisterRequestModel registerRequest, ClaimsPrincipal clientClaims)
        {
            try
            {
                var client = await _claimService.GetClientByClaim(clientClaims).ConfigureAwait(true);
                if(client == null)
                {
                    throw new Exception("There is no client for requested token");
                }

                if(client.UserType != UserType.Customer)
                {
                    throw new Exception("Only customer can do self register");
                }

                //get user 
                var exuser = await _userRepository.GetUser(registerRequest.UserName).ConfigureAwait(true);
                if(exuser != null)
                {
                    throw new Exception("Username already registered");
                }

                //check request role exist or not
                var role = await _roleRepository.GetCustomerRole().ConfigureAwait(true);
                if(role == null)
                {
                    throw new Exception("There are no role for requested id");
                }

                registerRequest.RoleId = Guid.Parse(role.Id);


                var baseOrigin = await _originRepository.GetCustomerOrigin().ConfigureAwait(true);
                registerRequest.OriginId = baseOrigin.Id;


                User user = new User();
                user.Id = Guid.NewGuid().ToString();
                user.FirstName = registerRequest.FirstName;
                user.LastName = registerRequest.LastName;
                user.UserName = registerRequest.UserName;
                user.Email = registerRequest.Email;
                user.IsTwoFactorEnabled = registerRequest.EnableTwofactor;
                user.PhoneNumber = registerRequest.UserName;
                user.UserType = client.UserType;

                var result = await StoreUser(user, registerRequest).ConfigureAwait(true);
                if (result == null)
                {
                    throw new Exception("Something went wrong, please try again later!");
                }


                if (!registerRequest.EnableTwofactor)
                {
                    await AddUserClaims(user, role, baseOrigin.Id).ConfigureAwait(true);
                    var response = await SignIn(user,client, baseOrigin);
                    response.UserId = Guid.Parse(user.Id);
                    return response;
                }
                else
                {
                    var res = await TwoFactorGenerater(user, client, TokenType.TowFactorRegisterToken, baseOrigin).ConfigureAwait(true);
                    res.UserId = Guid.Parse(user.Id);
                    return res;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public async Task<TokenReponseModel> TwoFactorValidation(TwoFactorValidationModel validationModel)
        {
            var principals =  await _tokenService.GetPrincipalFromToken(validationModel.Token, true).ConfigureAwait(true);
            var _userName = principals.FindFirst(DefinedClaims.Username)?.Value;
            var _userId = principals.FindFirst(DefinedClaims.UserId).Value;
            var _otp = principals.FindFirst(DefinedClaims.Otp).Value;
            var _tokenType = principals.FindFirst(DefinedClaims.TokenType).Value;
            var _originId = principals.FindFirst(DefinedClaims.OriginId).Value;

            var origin = await _originRepository.GetOrigin(Guid.Parse(_originId)).ConfigureAwait(true);

            var client = await _claimService.GetClientByClaim(principals).ConfigureAwait(true);
            if(client == null)
            {
                throw new Exception("There are no client for requwsted token");
            }


            if(_tokenType == TokenType.TowFactorLoginToken.ToString())
            {
                var user = await _userRepository.GetUser(Guid.Parse(_userId)).ConfigureAwait(true);
                if(user == null)
                {
                    throw new Exception("User not found");
                }

                var otpResult =_symmetricEncryption.verify(validationModel.Code, _otp);

                if(!otpResult)
                {
                    throw new Exception("Token verification failed!");
                }                         
                return await SignIn(user,client, origin).ConfigureAwait(true);            

            }
            else if(_tokenType == TokenType.TowFactorRegisterToken.ToString())
            {

                TempUserModel userCache = null;
                _memoryCache.TryGetValue(_userName, out userCache);

                if(userCache == null)
                {
                    throw new Exception("User not exist for requested Id");
                }


                var otpResult = _symmetricEncryption.verify(validationModel.Code, _otp);

                if (!otpResult)
                {
                    throw new Exception("Token verification failed!");
                }

                //store to database
                var result = await _userRepository.SaveUser(userCache.User as User, userCache.Password).ConfigureAwait(true);

                if (!result.Succeeded)
                {
                    throw new Exception("Can't register the user at the moment");
                }

                //check request role exist or not
                var role = await _roleRepository.GetRole(userCache.RoleId).ConfigureAwait(true);
                if (role == null)
                {
                    throw new Exception("There are no role for requested id");
                }

                var userExist = await _userRepository.GetUser(Guid.Parse(_userId)).ConfigureAwait(true);

                await AddUserClaims(userExist, role, userCache.OriginId).ConfigureAwait(true);

                var res = await SignIn(userExist, client, origin).ConfigureAwait(true);
                res.IsNewUser = true;
                res.UserId = Guid.Parse(userExist.Id);
                res.UserType = role.UserType;
                return res;
            }
            else
            {
                return null;
            }

        }

        public async Task<TokenReponseModel> TwoFactorGenerater(User user, Client client,  TokenType tokenType, Origin origin,bool sendSMS=true)
        {
            var otpValue = OtpHandler.GenerateOTPCode();

            if (sendSMS)
            {
                StringBuilder message = new StringBuilder();
                message.AppendFormat("Your activation code  is {0}.", otpValue);
                await _smsService.Send(user.UserName, message.ToString()).ConfigureAwait(true);
            }
            //send notifaciton

            var hashedOTP = _symmetricEncryption.Encrypt(otpValue);

            var otpClaims = await _claimService.GetOtpClaims(user, hashedOTP, tokenType, client, origin).ConfigureAwait(true);
            var otpToken = await _tokenService.GenerateAccessToken(otpClaims, tokenType).ConfigureAwait(true);
            var response = _tokenService.GetTokenResponse(otpToken, "", tokenType);
            response.OTP = otpValue;
            return response;
        }

        public async Task AddUserClaims(User user, Role role, Guid originId)
        {
            //assaign user to role
            var roleAddedResult = await _userRepository.AddRoleToUser(user, role).ConfigureAwait(true);
            if (roleAddedResult == null)
            {
                throw new Exception("User registered, but role can't be assaign for a moment");
            }

            var origin = await _originRepository.GetOrigin(originId).ConfigureAwait(true);
            if(origin == null)
            {
                throw new Exception("There are no origin for requested id");
            }

            var originAddedResult = await _originRepository.AddUserToOrigin(Guid.Parse(user.Id), originId).ConfigureAwait(true);
            if (originAddedResult == null)
            {
                throw new Exception("User registered, but origin can't be assaign for a moment");
            }
        }

        public async Task<User> StoreUser(User user, RegisterRequestModel registerRequestModel )
        {
            if (registerRequestModel.EnableTwofactor)
            {
                //store to webcache
                TempUserModel tempUser = new TempUserModel();
                tempUser.User = user;
                tempUser.Password = registerRequestModel.Password;
                tempUser.RoleId = (Guid)registerRequestModel.RoleId;
                tempUser.OriginId = (Guid)registerRequestModel.OriginId;

                var added = _memoryCache.Set(user.UserName, tempUser);
                return user;
            }
            else
            {
                //store to database
                var result = await _userRepository.SaveUser(user, registerRequestModel.Password).ConfigureAwait(true);
                if (!result.Succeeded)
                {
                    throw new Exception("User can't register at the moment");
                }
                else
                {
                    return user;
                }
            }
        }

        public async Task<TokenReponseModel> Login(LoginRequestModel loginRequest, ClaimsPrincipal clientClaims)
        {

            var client = await _claimService.GetClientByClaim(clientClaims).ConfigureAwait(true);
            if (client == null)
            {
                throw new Exception("There are no client for regqusted token");
            }

            //get user
            var user = await _userRepository.GetUser(loginRequest.UserName).ConfigureAwait(true);
            if(user == null)
            {
                //ToDo: Create Unauthorized Exception
                throw new Exception("There are no user for requested username");
            }

            if(client.UserType != user.UserType)
            {
                throw new Exception("This client can't access the another client user");
            }

            var result = await _userRepository.SingInUser(user, loginRequest.Password).ConfigureAwait(true);
            if (!result.Succeeded)
            {
                throw new Exception("Incorrect Password");
            }

            var userOrigins = await _originRepository.GetUserOrigins(Guid.Parse(user.Id)).ConfigureAwait(true);


            if (user.IsTwoFactorEnabled)
            {
                return await TwoFactorGenerater(user, client, TokenType.TowFactorLoginToken, userOrigins.FirstOrDefault()).ConfigureAwait(true);
            }
            else
            {
                return await SignIn(user, client, userOrigins.FirstOrDefault());

            }


        }

        public async Task<TokenReponseModel>  RefreshToken(RefreshTokenRequestModel tokenRequestModel)
        {
            //get token from db
            //token validation
            var principles = await _tokenService.GetPrincipalFromToken(tokenRequestModel.Token, false);
            if (principles == null)
            {
                throw new Exception("Invalid Token");
            }

            var client = await _claimService.GetClientByClaim(principles).ConfigureAwait(true);
            if (client == null)
            {
                throw new Exception("There are no client for regqusted token");
            }

            var _tokenType = principles.FindFirst(DefinedClaims.TokenType).Value;

            if(_tokenType.ParseEnum<TokenType>() != TokenType.LoginToken)
            {
                throw new Exception("User must login with login access token");
            }

            var _username = principles.FindFirst(DefinedClaims.Username).Value;

            var user = await _userRepository.GetUser(_username).ConfigureAwait(true);
            if(user == null)
            {
                throw new Exception("User not found for requsted token");
            }

            var tokenResult = await  _userTokenRepository.GetUserToken(tokenRequestModel.Token).ConfigureAwait(true);
            if(tokenResult == null)
            {
                throw new Exception("Token Invalid, please login again");
            }

            if(tokenResult.RefreshToken != tokenRequestModel.RefreshToken)
            {
                throw new Exception("Invalid refresh token");
            }
            await _userTokenRepository.RemoveToken(tokenRequestModel.Token);

            var _originId = principles.FindFirst(DefinedClaims.OriginId).Value;

            var origin = await _originRepository.GetOrigin(Guid.Parse(_originId)).ConfigureAwait(true);

            return await SignIn(user, client, origin);

        }

        private async Task<TokenReponseModel> SignIn(User user, Client client, Origin origin)
        {
            var profileClaims = await _claimService.GetProfileClaims(user, client, TokenType.LoginToken, origin).ConfigureAwait(true);
            var accessToken = await _tokenService.GenerateAccessToken(profileClaims, TokenType.LoginToken).ConfigureAwait(true);
            var refreshToken = await _tokenService.GenerateRefreshToken();

            UserToken userToken = new UserToken();
            userToken.CreatedTime = DateTime.UtcNow;
            userToken.RefreshToken = refreshToken;
            userToken.Token = accessToken;
            userToken.UserId = user.Id;
            userToken.ClientId = client.Id;

            await _userTokenRepository.Add(userToken).ConfigureAwait(true);

            var result = _tokenService.GetTokenResponse(accessToken, refreshToken, TokenType.LoginToken);
            result.UserId = Guid.Parse(user.Id);
            result.UserType = user.UserType;
            return result;
        }

        public async Task<TokenReponseModel> ReSendOtp(string userName, string oldToken, ClaimsPrincipal clientClaims)
        {
            var client = await _claimService.GetClientByClaim(clientClaims).ConfigureAwait(true);

            if (client == null)
            {
                throw new Exception("There are no client for regqusted token");
            }
            var user =  await _userRepository.GetUser(userName).ConfigureAwait(true);
            if(user == null)
            {
                throw new Exception("User not found for requested username");
            }

            var principles = await _tokenService.GetPrincipalFromToken(oldToken, false);
            if (principles == null)
            {
                throw new Exception("Invalid Token");
            }

            var _tokenType = principles.FindFirst(DefinedClaims.TokenType).Value;

            var _originId = clientClaims.FindFirst(DefinedClaims.OriginId).Value;

            var origin = await _originRepository.GetOrigin(Guid.Parse(_originId)).ConfigureAwait(true);

            var result = await TwoFactorGenerater(user, client, _tokenType.ParseEnum<TokenType>(), origin).ConfigureAwait(true);
            return result;
        }

        public async Task<TokenReponseModel> ForgotPassword(string userName, ClaimsPrincipal clientClaims)
        {
            var client = await _claimService.GetClientByClaim(clientClaims).ConfigureAwait(true);

            if (client == null)
            {
                throw new Exception("There are no client for regqusted token");
            }

            var user = await _userRepository.GetUser(userName).ConfigureAwait(true);
            if(user == null)
            {
                throw new Exception("User not found for requested username");
            }

            var userOrigin = await _originRepository.GetUserOrigins(Guid.Parse(user.Id)).ConfigureAwait(true);


            var result = await TwoFactorGenerater(user, client, TokenType.ForgotPasswordVerificationToken, userOrigin.FirstOrDefault()).ConfigureAwait(true);
            return result;

        }

        public async Task<TokenReponseModel> ForgotPassword(string otp, string token)
        {
            var principles = await _tokenService.GetPrincipalFromToken(token, false);
            if (principles == null)
            {
                throw new Exception("Invalid Token");
            }

            var client = await _claimService.GetClientByClaim(principles).ConfigureAwait(true);

            if (client == null)
            {
                throw new Exception("There are no client for regqusted token");
            }

            var _tokenType = principles.FindFirst(DefinedClaims.TokenType).Value;
            var _userName = principles.FindFirst(DefinedClaims.Username).Value;

            if(_tokenType.ParseEnum<TokenType>() != TokenType.ForgotPasswordVerificationToken)
            {
                throw new Exception("Invalid Token");
            }

            var user = await _userRepository.GetUser(_userName).ConfigureAwait(true);
            if (user == null)
            {
                throw new Exception("User not found for requested token");
            }

            var _otp = principles.FindFirst(DefinedClaims.Otp).Value;

            var isValidOTP =  _symmetricEncryption.verify(otp, _otp);

            if (!isValidOTP)
            {
                throw new Exception("Token verification failed!");
            }

            var _originId = principles.FindFirst(DefinedClaims.OriginId).Value;

            var origin = await _originRepository.GetOrigin(Guid.Parse(_originId)).ConfigureAwait(true);

            var result = await TwoFactorGenerater(user, client, TokenType.ForgotPasswordToken, origin,false).ConfigureAwait(true);
            return result;

        }
         
        public async Task<TokenReponseModel> ForgotPassword(ForgotPasswordRequestModel forgotPassword)
        {
            var principles = await _tokenService.GetPrincipalFromToken(forgotPassword.Token, false);
            if (principles == null)
            {
                throw new Exception("Invalid Token");
            }

            var client = await _claimService.GetClientByClaim(principles).ConfigureAwait(true);

            if (client == null)
            {
                throw new Exception("There are no client for regqusted token");
            }

            var _tokenType = principles.FindFirst(DefinedClaims.TokenType).Value;
            var _userName = principles.FindFirst(DefinedClaims.Username).Value;

            if (_tokenType.ParseEnum<TokenType>() != TokenType.ForgotPasswordToken)
            {
                throw new Exception("Invalid Token");
            }

            var user = await _userRepository.GetUser(_userName).ConfigureAwait(true);
            if (user == null)
            {
                throw new Exception("User not found for requested token");
            }

            var result = await _userRepository.ResetPassword(user, forgotPassword.NewPassword).ConfigureAwait(true);
            if (!result.Succeeded)
            {
                throw new Exception("Password reset failed, please try again later!");
            }

            var _originId = principles.FindFirst(DefinedClaims.OriginId).Value;

            var origin = await _originRepository.GetOrigin(Guid.Parse(_originId)).ConfigureAwait(true);

            return await SignIn(user, client, origin).ConfigureAwait(true);

        }

        public async Task<List<string>> GetAllAdminUsers()
        {
            var result = await _userRepository.GetAllActiveAdminUsers();
            return result;
        }

    }
}
