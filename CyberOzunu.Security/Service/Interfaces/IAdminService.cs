﻿using CyberOzunu.Security.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Service.Interfaces
{
    public interface IAdminService
    {
        Task<UserModel> AddAdminUser(RegisterRequestModel registerRequest, ClaimsPrincipal clientClaims);
        Task<OriginModel> AddOrigin(OriginModel originModel);
        Task<UserModel> AddSubContractorUser(RegisterRequestModel register, ClaimsPrincipal User);
    }
}
