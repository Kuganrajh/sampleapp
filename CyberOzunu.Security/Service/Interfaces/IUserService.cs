﻿using CyberOzunu.Security.Entity;
using CyberOzunu.Security.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Service.Interfaces
{
    public interface IUserService
    {
        Task<TokenReponseModel> Login(LoginRequestModel loginRequest, ClaimsPrincipal clientClaims);
        Task<TokenReponseModel> RegisterUser(RegisterRequestModel registerRequest, ClaimsPrincipal clientClaims);
        Task<TokenReponseModel> TwoFactorValidation(TwoFactorValidationModel validationModel);
        Task<TokenReponseModel> RefreshToken(RefreshTokenRequestModel tokenRequestModel);
        Task AddUserClaims(User user, Role role, Guid originId);
        Task<User> StoreUser(User user, RegisterRequestModel registerRequestModel);
        Task<List<string>> GetAllAdminUsers();

        Task<TokenReponseModel> ForgotPassword(string userName, ClaimsPrincipal clientClaims);

        Task<TokenReponseModel> ForgotPassword(string otp, string token);

        Task<TokenReponseModel> ForgotPassword(ForgotPasswordRequestModel forgotPassword);
    }
}
