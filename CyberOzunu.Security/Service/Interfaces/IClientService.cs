﻿using CyberOzunu.Security.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Service.Interfaces
{
    public interface IClientService
    {
        Task<TokenReponseModel> GetClientToken(Guid id, string key);
    }
}
