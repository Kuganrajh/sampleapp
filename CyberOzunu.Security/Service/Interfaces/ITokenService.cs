﻿using CyberOzunu.Security.Config;
using CyberOzunu.Security.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Service.Interfaces
{
    public interface ITokenService
    {
        TokenReponseModel GetTokenResponse(string token, string refreshtoken, TokenType tokenType);
        Task<ClaimsPrincipal> GetPrincipalFromToken(string token, bool checkLifeTime);
        Task<string> GenerateRefreshToken();
        Task<string> GenerateAccessToken(IEnumerable<Claim> claims, TokenType tokenType);
    }
}
 