﻿using CyberOzunu.Security.Config;
using CyberOzunu.Security.Entity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Service.Interfaces
{
    public interface IClaimService
    {
        Task<IEnumerable<Claim>> GetProfileClaims(User user, Client client, TokenType tokenType, Origin origin);
        Task<IEnumerable<Claim>> GetOtpClaims(User user, string otpCode, TokenType tokenType, Client client, Origin origin);
        Task<Client> GetClientByClaim(ClaimsPrincipal claims);
        Task<IEnumerable<Claim>> GetClientClaims(Client client);
        Task<IEnumerable<Claim>> GetQuotationClaims(string phonenumber, string email, string otpCode, string firstName, string lastName, ClaimsPrincipal claim);
    }
}
