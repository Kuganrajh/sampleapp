﻿using CyberOzunu.Security.Config;
using CyberOzunu.Security.Models;
using CyberOzunu.Security.Service.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Service
{
    public class TokenService : ITokenService
    {
        private readonly IConfiguration _configuration;
        private readonly TokenSetting _tokenSetting;

        public TokenService(IConfiguration configuration, TokenSetting tokenSetting)
        {
            _configuration = configuration;
            _tokenSetting = tokenSetting;
        }
        public async Task<string> GenerateAccessToken(IEnumerable<Claim> claims, TokenType tokenType)
        {
            try
            {
                var durtion = await GetTokenDurationByTokenType(tokenType).ConfigureAwait(true) ;

                var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration.GetSection("JWT").GetSection("Key").Value));
                var jwtToken = new JwtSecurityToken(issuer: _configuration.GetSection("JWT").GetSection("issuer").Value,
                    audience: _configuration.GetSection("JWT").GetSection("audience").Value,
                    claims: claims,
                    notBefore: DateTime.UtcNow,
                    expires: DateTime.UtcNow.AddMinutes(durtion),
                    signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature)
                );

                return new JwtSecurityTokenHandler().WriteToken(jwtToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<string> GenerateRefreshToken()
        {
            int length = 32;
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public async Task<ClaimsPrincipal> GetPrincipalFromToken(string token, bool checkLifeTime)
        {

            try
            {
                var tokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration.GetSection("JWT").GetSection("Key").Value)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = checkLifeTime
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                SecurityToken securityToken;



                var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);

                if (principal == null)
                    return null;

                var jwtSecurityToken = securityToken as JwtSecurityToken;
                if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256Signature, StringComparison.InvariantCultureIgnoreCase))
                    throw new SecurityTokenException("Invalid token");
                return principal;
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

        public TokenReponseModel GetTokenResponse( string token, string refreshtoken, TokenType tokenType)
        {
            TokenReponseModel createTokenModel = new TokenReponseModel()
            {
                TokenType = tokenType,
                Token = token,
                RefreshToken = refreshtoken
            };

            return createTokenModel;
        }

        private async Task<double> GetTokenDurationByTokenType(TokenType tokenType)
        {
            switch (tokenType)
            {
                case TokenType.LoginToken:
                    {
                        return double.Parse(_tokenSetting.LoginTokenDuration);
                    }
                case TokenType.TowFactorLoginToken:
                    {
                        return double.Parse(_tokenSetting.TwoFactorLoginTokenDuration);
                    }
                case TokenType.TowFactorRegisterToken:
                    {
                        return double.Parse(_tokenSetting.TwoFactorRegisterTokenDuration);
                    }
                case TokenType.ClientAccessToken:
                    {
                        return double.Parse(_tokenSetting.LoginTokenDuration);
                    }
                case TokenType.QuotationRequestToken:
                    {
                        return double.Parse(_tokenSetting.TwoFactorLoginTokenDuration);
                    }
                default:
                    return 0.0;
            }
        }
    }
}
