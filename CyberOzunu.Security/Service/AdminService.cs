﻿using CyberOzunu.Security.Config;
using CyberOzunu.Security.Entity;
using CyberOzunu.Security.Models;
using CyberOzunu.Security.Repository.Interfaces;
using CyberOzunu.Security.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Service
{
    public class AdminService : IAdminService
    {
        private readonly IClaimService _claimService;
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IUserService _userService;
        private readonly IOriginRepository _originRepository;

        public AdminService(IOriginRepository originRepository, IClaimService claimService, IUserRepository userRepository, IRoleRepository roleRepository, IUserService userService)
        {
            _claimService = claimService;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _userService = userService;
            _originRepository = originRepository;
        }

        public async Task<UserModel> AddSubContractorUser(RegisterRequestModel register, ClaimsPrincipal User)
        {
            var client = await _claimService.GetClientByClaim(User).ConfigureAwait(true);
            if (client == null)
            {
                throw new Exception("There are no client for regqusted token");
            }

            //get user 
            var exuser = await _userRepository.GetUser(register.UserName).ConfigureAwait(true);
            if (exuser != null)
            {
                throw new Exception("Username already registered");
            }

            if (client.UserType != UserType.Admin && client.UserType  != UserType.Supplier)
            {
                throw new Exception("Requested client can't use this api");
            }

            if(client.UserType == UserType.Admin && register.OriginId == null)
            {
                throw new Exception("Origin Id must needed");
            }
            else
            {
                register.OriginId = Guid.Parse(User.FindFirst(DefinedClaims.OriginId).Value);
            }

            if(client.UserType == UserType.Admin && register.RoleId == null)
            {
                var roleResult =  await _roleRepository.GetOriginParentRole((Guid)register.OriginId).ConfigureAwait(true);
                if(roleResult == null)
                {
                    Role role1 = new Role();
                    role1.IsParentRole = true;
                    role1.Name = "Supplier Admin";
                    role1.NormalizedName = "Supplier_Admin_"+ register.OriginId.ToString();
                    role1.ConcurrencyStamp = Guid.NewGuid().ToString();
                    role1.OriginId = (Guid)register.OriginId;
                    role1.UserType = UserType.Supplier;

                   roleResult = await _roleRepository.Add(role1).ConfigureAwait(true);
                }
                register.RoleId = Guid.Parse(roleResult.Id);
            }

            if(client.UserType == UserType.Supplier && register.RoleId == null)
            {
                throw new Exception("Role id is Requestesd");
            }

            //check request role exist or not
            var role = await _roleRepository.GetRole((Guid)register.RoleId).ConfigureAwait(true);
            if (role == null)
            {
                throw new Exception("There are no role for requested id");
            }


            User user = new User();
            user.Id = Guid.NewGuid().ToString();
            user.FirstName = register.FirstName;
            user.LastName = register.LastName;
            user.UserName = register.UserName;
            user.Email = register.Email;
            user.IsTwoFactorEnabled = false;
            user.PhoneNumber = register.UserName;
            user.UserType = client.UserType;

            var result = await _userService.StoreUser(user, register).ConfigureAwait(true);
            if (result == null)
            {
                throw new Exception("Something went wrong, please try again later!");
            }

            var originBase = await _originRepository.GetOrigin((Guid)register.OriginId).ConfigureAwait(true);

            await _userService.AddUserClaims(user, role, originBase.Id).ConfigureAwait(true);


            //return userModel
            UserModel userModel = new UserModel();
            userModel.Id = Guid.Parse(result.Id);
            userModel.UserName = result.UserName;
            userModel.FirstName = result.FirstName;
            userModel.LastName = result.LastName;
            userModel.UserType = result.UserType;
            userModel.Phonenumber = result.PhoneNumber;
            userModel.Email = result.Email;
            userModel.Roles = new List<RoleModel>() { new RoleModel() { Id = Guid.Parse(role.Id), Name = role.Name } };

            return userModel;
        }


        public async Task<OriginModel> AddOrigin(OriginModel  originModel)
        {
            var origin = new Origin()
            {   Id = originModel.Id ?? Guid.NewGuid(),
                Name = originModel.Name,
                UserType = originModel.UserType

            };

            var result = await _originRepository.Add(origin).ConfigureAwait(true);

            var response = new OriginModel()
            {
                Id = result.Id,
                Name = result.Name,
                UserType = result.UserType

            };

            return response;
        }


        public async Task<UserModel> AddAdminUser(RegisterRequestModel registerRequest, ClaimsPrincipal clientClaims)
        {
            try
            {
                var client = await _claimService.GetClientByClaim(clientClaims).ConfigureAwait(true);
                if (client == null)
                {
                    throw new Exception("There are no client for regqusted token");
                }

                if (client.UserType != UserType.Admin)
                {
                    throw new Exception("Admin client only use this api");
                }

                var _tokenType = clientClaims.FindFirst(DefinedClaims.TokenType).Value;

                if(_tokenType != TokenType.LoginToken.ToString())
                {
                    throw new Exception("Admin user must to login");
                }

                if (registerRequest.RoleId == null)
                {
                    throw new Exception("Role id request");
                }

                //get user 
                var exuser = await _userRepository.GetUser(registerRequest.UserName).ConfigureAwait(true);
                if (exuser != null)
                {
                    throw new Exception("Username already registered");
                }

                //check request role exist or not
                var role = await _roleRepository.GetRole((Guid)registerRequest.RoleId).ConfigureAwait(true);
                if (role == null)
                {
                    throw new Exception("There are no role for requested id");
                }


                User user = new User();
                user.Id = Guid.NewGuid().ToString();
                user.FirstName = registerRequest.FirstName;
                user.LastName = registerRequest.LastName;
                user.UserName = registerRequest.UserName;
                user.Email = registerRequest.Email;
                user.IsTwoFactorEnabled = false;
                user.PhoneNumber = registerRequest.UserName;
                user.UserType = client.UserType;

                var result = await _userService.StoreUser(user, registerRequest).ConfigureAwait(true);
                if (result == null)
                {
                    throw new Exception("Something went wrong, please try again later!");
                }

                var originBase = await _originRepository.GetBaseOrigin().ConfigureAwait(true);

                await _userService.AddUserClaims(user, role, originBase.Id).ConfigureAwait(true);


                //return userModel
                UserModel userModel = new UserModel();
                userModel.Id = Guid.Parse(result.Id);
                userModel.UserName = result.UserName;
                userModel.FirstName = result.FirstName;
                userModel.LastName = result.LastName;
                userModel.UserType = result.UserType;
                userModel.Phonenumber = result.PhoneNumber;
                userModel.Email = result.Email;
                userModel.Roles = new List<RoleModel>() { new RoleModel() { Id = Guid.Parse(role.Id), Name = role.Name } };

                return userModel;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
