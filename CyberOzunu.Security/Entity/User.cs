﻿using CyberOzunu.Security.Config;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CyberOzunu.Security.Entity
{
    public class User : IdentityUser<string>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsTwoFactorEnabled { get; set; }
        public UserType UserType { get; set; }
        public List<OriginUser> Origins { get; set; }
        public bool IsActive { get; set; }
    }
}
