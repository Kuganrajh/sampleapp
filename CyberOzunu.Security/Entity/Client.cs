﻿using CyberOzunu.Security.Config;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Entity
{
    public class Client
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public UserType UserType { get; set; }
        public virtual List<UserToken> Tokens { get; set; }
    }
}
