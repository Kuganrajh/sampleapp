﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Entity
{
    public class OriginUser
    {
        public Guid Id { get; set; }
        public Guid OriginId { get; set; }
        public virtual Origin Origin { get; set; }

        public string UserId { get; set; }
        public virtual User User { get; set; }
    }
}
