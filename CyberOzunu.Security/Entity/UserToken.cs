﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Entity
{
    public class UserToken
    {
        public Guid Id { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public string UserId { get; set; }
        public Guid ClientId { get; set; }
        public DateTime CreatedTime { get; set; }
        
        public virtual Client Client { get; set; }
        public virtual User User { get; set; }
    }
}
