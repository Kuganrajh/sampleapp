﻿using CyberOzunu.Security.Config;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Entity
{
    public class Origin 
    {
        public Guid Id { get; set; }
        public string Name { get;set; }
        public UserType UserType { get; set; }
        public List<OriginUser> Users { get; set; }
    }
}
