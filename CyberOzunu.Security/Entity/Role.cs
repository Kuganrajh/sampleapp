﻿using CyberOzunu.Security.Config;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Entity
{
    public class Role : IdentityRole
    {
        public UserType UserType { get; set; }
        public Guid OriginId { get; set; }
        public virtual Origin Origin { get; set; }
        public bool IsParentRole { get; set; }
    }
}
