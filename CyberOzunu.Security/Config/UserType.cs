﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Config
{
    public enum UserType
    {
        Admin = 1,
        Customer = 2,
        Supplier = 3
    }
}
