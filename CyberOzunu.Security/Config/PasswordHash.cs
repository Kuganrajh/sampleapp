﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Config
{
    public class PasswordHash
    {
        public async Task<string> HashPassword(string key)
        {
            return new PasswordHasher<object>().HashPassword(null, key);
        }

        public async Task<PasswordVerificationResult> VerifyPassowrd(string requestedKey, string storedKey)
        {
            var verificationResult = new PasswordHasher<object>().VerifyHashedPassword(null, storedKey, requestedKey);
            return verificationResult;
        }
    }
}