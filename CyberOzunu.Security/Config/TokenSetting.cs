﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Config
{
    public class TokenSetting
    {
        public string LoginTokenDuration { get; set; }
        public string TwoFactorLoginTokenDuration { get; set; }
        public string TwoFactorRegisterTokenDuration { get; set; }
    }
}
