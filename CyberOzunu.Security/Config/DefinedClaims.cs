﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Config
{
    public static class DefinedClaims
    {
        public const string UserId = "UserId";
        public const string FirstName = "FirstName";
        public const string LastName = "LastName";
        public const string PhoneNumber = "PhoneNumber";
        public const string Role = "Role";
        public const string RoleId = "RoleId";
        public const string Username = "Username";
        public const string Otp = "OTP";
        public const string TokenType = "TokenType";
        public const string UserType = "UserType";
        public const string Email = "Email";

        public const string ClientId = "ClientId";
        public const string ClientName = "ClientName";

        public const string OriginId = "OriginId";
        public const string OriginName = "OriginName";
    }
}
