﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Config
{
    public enum TokenType
    {
        LoginToken = 1,
        TowFactorLoginToken = 2,
        TowFactorRegisterToken = 3,
        ForgotPasswordToken = 4,
        ForgotPasswordVerificationToken = 5,
        ClientAccessToken = 6,
        QuotationRequestToken = 7
    }
}
