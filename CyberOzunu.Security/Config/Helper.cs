﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Config
{
    public static class Helper
    {
        public static T ParseEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
