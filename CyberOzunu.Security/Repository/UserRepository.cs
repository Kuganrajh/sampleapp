﻿using CyberOzunu.Security.Data;
using CyberOzunu.Security.Entity;
using CyberOzunu.Security.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ApplicationDbContext _dbContext;

        public UserRepository(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationDbContext dbContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _dbContext = dbContext;
        }


        public async Task<IdentityUserRole<string>> AddRoleToUser(User user, Role role)
        {
            IdentityUserRole<string> identityUserRole = new Microsoft.AspNetCore.Identity.IdentityUserRole<string>();
            identityUserRole.UserId = user.Id;
            identityUserRole.RoleId = role.Id;
            var roleAdded = _dbContext.UserRoles.Add(identityUserRole);
            _dbContext.SaveChanges();
            return roleAdded.Entity;
        }

        public async Task<IdentityResult> SaveUser(User user, string password)
        {
            var userRegistred = await _userManager.CreateAsync(user, password).ConfigureAwait(true);
            return userRegistred;
        }

        public async Task<User> GetUser(string username)
        {
            var user = await  _userManager.FindByNameAsync(username).ConfigureAwait(true);
            return user;
        }

        public async Task<User> GetUser(Guid Id)
        {
            var user = await _userManager.FindByIdAsync(Id.ToString()).ConfigureAwait(true);
            return user;
        }

        public async Task<SignInResult> SingInUser(User user, string password)
        {
            var result = await _signInManager.PasswordSignInAsync(user, password, false, false).ConfigureAwait(true);
            return result;
        }

        public async Task<IdentityResult> ResetPassword(User user, string newPassword)
        {
            string newPasswordhash = _userManager.PasswordHasher.HashPassword(user, newPassword);
            user.PasswordHash = newPasswordhash;
            var result = await _userManager.UpdateAsync(user).ConfigureAwait(true);
            return result;
           
        }

        public async Task<List<string>> GetAllActiveAdminUsers()
        {
            var result = await _dbContext.Users.Where(p => p.UserType == Config.UserType.Admin && p.IsActive).Select(p=>p.Id).ToListAsync();
            return result;
        }
    }
}
