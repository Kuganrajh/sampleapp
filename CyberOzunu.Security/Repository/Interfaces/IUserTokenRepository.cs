﻿using CyberOzunu.Security.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Repository.Interfaces
{
    public interface IUserTokenRepository
    {
        Task<List<UserToken>> GetUserTokens(string userId);
        Task<UserToken> GetUserToken(string token);
        Task<UserToken> Add(UserToken usertoken);
        Task RemoveToken(string token);
    }
}
