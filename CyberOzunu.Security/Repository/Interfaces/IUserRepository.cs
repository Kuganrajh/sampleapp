﻿using CyberOzunu.Security.Entity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Repository.Interfaces
{
    public interface IUserRepository
    {
        Task<SignInResult> SingInUser(User user, string password);
        Task<User> GetUser(string username);
        Task<User> GetUser(Guid Id);
        Task<IdentityResult> SaveUser(User user, string password);
        Task<IdentityUserRole<string>> AddRoleToUser(User user, Role role);
        Task<IdentityResult> ResetPassword(User user, string newPassword);
        Task<List<string>> GetAllActiveAdminUsers();

    }
}
