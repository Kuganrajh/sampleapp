﻿using CyberOzunu.Security.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Repository.Interfaces
{
    public interface IClientRepository
    {
        Task<Client> GetClient(Guid id);
        Task<Client> ClientVerification(Guid id, string key);
    }
}
