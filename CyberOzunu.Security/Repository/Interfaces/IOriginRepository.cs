﻿using CyberOzunu.Security.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Repository.Interfaces
{
    public interface IOriginRepository
    {
        Task<Origin> GetOrigin(Guid id);
        Task<OriginUser> AddUserToOrigin(Guid userId, Guid originId);
        Task<Origin> GetBaseOrigin();
        Task<Origin> Add(Origin origin);
        Task<List<Origin>> GetUserOrigins(Guid userId);
        Task<Origin> GetCustomerOrigin();
    }
}
