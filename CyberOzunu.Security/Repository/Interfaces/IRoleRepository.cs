﻿using CyberOzunu.Security.Entity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Repository.Interfaces
{
    public interface IRoleRepository
    {
        Task<Role> GetRole(Guid id);
        Task<List<Role>> GetUserRole(Guid userId, Client client);
        Task<Role> GetCustomerRole();
        Task<Role> GetOriginParentRole(Guid originId);
        Task<Role> Add(Role role);
    }
}
