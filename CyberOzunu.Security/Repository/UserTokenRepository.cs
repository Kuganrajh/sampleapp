﻿using CyberOzunu.Security.Data;
using CyberOzunu.Security.Entity;
using CyberOzunu.Security.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Repository
{
    public class UserTokenRepository : IUserTokenRepository
    {
        private readonly ApplicationDbContext _dBContext;

        public UserTokenRepository(ApplicationDbContext dBContext)
        {
            _dBContext = dBContext;
        }

        public async Task<UserToken> Add(UserToken usertoken)
        {
            var userTokensRemove = _dBContext.UserTokens.Where(x=>x.UserId == usertoken.UserId).OrderByDescending(o=>o.CreatedTime).Skip(2).ToList();
            if(userTokensRemove?.Count > 0)
            {
                _dBContext.UserTokens.RemoveRange(userTokensRemove);
            }
            var userTokenResult =  _dBContext.UserTokens.Add(usertoken);
            _dBContext.SaveChanges();
            return userTokenResult.Entity;
        }

        public async Task<UserToken> GetUserToken(string token)
        {
            var userToken = _dBContext.UserTokens.SingleOrDefault(x=>x.Token == token);
            return userToken;
        }

        public async Task RemoveToken(string token)
        {
            var userToken = _dBContext.UserTokens.SingleOrDefault(x=>x.Token == token);
            if(userToken != null)
            {
                _dBContext.UserTokens.Remove(userToken);
                _dBContext.SaveChanges();
            }
        }

        public async Task<List<UserToken>> GetUserTokens(string userId)
        {
            var userTokens = _dBContext.UserTokens.Where(x=>x.UserId == userId).ToList();
            return userTokens;
        }
    }
}
