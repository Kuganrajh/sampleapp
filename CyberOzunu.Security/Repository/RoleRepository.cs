﻿using CyberOzunu.Security.Data;
using CyberOzunu.Security.Entity;
using CyberOzunu.Security.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Repository
{
    public class RoleRepository : IRoleRepository
    {
        private readonly ApplicationDbContext _dBContext;
        private readonly RoleManager<Role> _roleManager;

        public RoleRepository(ApplicationDbContext dBContext, RoleManager<Role> roleManager)
        {
            _dBContext = dBContext;
            _roleManager = roleManager;
        }

        public async Task<Role> Add(Role role)
        {
            var data = _dBContext.Roles.Add(role);
            _dBContext.SaveChanges();
            return data.Entity;
        }

        public async Task<Role> GetOriginParentRole(Guid originId)
        {
            return _dBContext.Roles.Where(x => x.OriginId == originId && x.IsParentRole).FirstOrDefault();
        }


        public async Task<Role> GetCustomerRole()
        {
            var role = _dBContext.Roles.Where(x => x.UserType == Config.UserType.Customer).FirstOrDefault();
            return role;
        }

        public async Task<Role> GetRole(Guid id)
        {
            var role = _dBContext.Roles.SingleOrDefault(x=>x.Id == id.ToString());
            return role;
        }

        public async Task<List<Role>> GetUserRole(Guid userId, Client client)
        {
            var userRoles = (from userRole in _dBContext.UserRoles
                            join role in _dBContext.Roles on userRole.RoleId equals role.Id
                            where
                            userRole.UserId == userId.ToString()
                            &&
                            role.UserType == client.UserType
                            select role).ToList();

            return userRoles;

        }
    }
}
