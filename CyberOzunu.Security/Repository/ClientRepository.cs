﻿using CyberOzunu.Security.Data;
using CyberOzunu.Security.Entity;
using CyberOzunu.Security.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.Security.Repository
{
    public class ClientRepository : IClientRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public ClientRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Client> GetClient(Guid id) 
        {
            var client = _dbContext.Clients.Select(x=> new Client() { Id = x.Id, UserType = x.UserType, Name = x.Name }).SingleOrDefault(x=>x.Id == id);
            return client;
        }

        public async Task<Client> ClientVerification(Guid id, string key)
        {
            var client = _dbContext.Clients.Where(x => x.Id == id && x.Key == key).Select(x => new Client() { Id = x.Id, Name = x.Name, UserType = x.UserType }).FirstOrDefault();
            return client;
        }
    }
}
