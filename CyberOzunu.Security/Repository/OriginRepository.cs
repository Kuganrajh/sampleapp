﻿using CyberOzunu.Security.Data;
using CyberOzunu.Security.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using CyberOzunu.Security.Repository.Interfaces;

namespace CyberOzunu.Security.Repository
{
    public class OriginRepository : IOriginRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public OriginRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Origin> GetCustomerOrigin()
        {
            return _dbContext.Origins.SingleOrDefault(x => x.UserType == Config.UserType.Customer);
        }

        public async Task<Origin> GetBaseOrigin()
        {
            return _dbContext.Origins.SingleOrDefault(x => x.UserType == Config.UserType.Admin);
        }

        public async Task<Origin> Add(Origin origin)
        {
            var data  = _dbContext.Origins.Add(origin);
            _dbContext.SaveChanges();

            return data.Entity;
        }

        public async Task<Origin> GetOrigin(Guid id)
        {
            var origin = _dbContext.Origins.SingleOrDefault(x=>x.Id == id);
            return origin;
        }


        public async Task<OriginUser> AddUserToOrigin(Guid userId, Guid originId)
        {
            OriginUser originUser = new OriginUser();
            originUser.UserId = userId.ToString();
            originUser.OriginId = originId;

            var data = _dbContext.OriginUsers.Add(originUser);
            _dbContext.SaveChanges();
            return data.Entity;
        }

        public async Task<List<Origin>> GetUserOrigins(Guid userId)
        {
            try
            {
                var data = (from origin in _dbContext.Origins
                            join originUser in _dbContext.OriginUsers on origin.Id equals originUser.OriginId
                            where
                            originUser.UserId == userId.ToString()
                            select origin).ToList();

                return data;
            }
            catch (Exception ex)
            {

                throw;
            }

            
        }


    }
}
