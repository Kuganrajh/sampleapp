﻿using CyberOzunu.Security.Config;
using CyberOzunu.Security.Data;
using CyberOzunu.Security.Entity;
using CyberOzunu.Security.Persistence;
using CyberOzunu.Security.Repository;

using CyberOzunu.Security.Repository.Interfaces;
using CyberOzunu.Security.Service;
using CyberOzunu.Security.Service.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security
{
    public static class Extension
    {
        public static void EnableAuth(this IServiceCollection services, byte[] key)
        {
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateActor = false,
                        ValidateLifetime = true
                    };
                });

        }

        public static void UseAuth(this IApplicationBuilder builder)
        {
            builder.UseAuthentication();
        }

        public static void AddAuth(this IServiceCollection services, IConfiguration configuration)
        {
            //dbcontext
            services.AddPersistence();

            services.AddMemoryCache();

            //add repository
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserTokenRepository, UserTokenRepository>();
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<IOriginRepository, OriginRepository>();

            //add services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IClaimService, ClaimService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<PasswordHash>();


            //Add Identity
            services.AddIdentity<User, Role>(
                config =>
                {
                    config.Password.RequireDigit = true;
                    config.Password.RequireLowercase = true;
                    config.Password.RequireUppercase = false;
                    config.Password.RequireNonAlphanumeric = true;
                    config.Password.RequiredLength = 8;

                }
             )
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();

            //Duration configuration
            var durationSetting = configuration.GetSection("DurationSetting").Get<TokenSetting>();
            services.AddSingleton(durationSetting);


        }

        public static void UpdateSecurityDatabase(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>())
                {
                    context.Database.Migrate();
                }
            }
        }

    }
}
