﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Models
{
    public class RegisterRequestModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool EnableTwofactor { get; set; }
        public Guid? RoleId { get; set; }
        public Guid? OriginId { get; set; }
    }
}
