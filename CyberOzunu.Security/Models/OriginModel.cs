﻿using CyberOzunu.Security.Config;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Models
{
    public class OriginModel
    {
        public Guid? Id { get; set; }
        public String Name { get; set; }
        public UserType UserType { get; set; }
    }
}
