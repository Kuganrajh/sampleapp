﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Models
{
    public class TwoFactorValidationModel
    {
        public string Token { get; set; }
        public string Code { get; set; }
    }
}
