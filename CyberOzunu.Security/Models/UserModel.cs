﻿using CyberOzunu.Security.Config;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Models
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserType UserType { get; set; }
        public string Phonenumber { get; set; }
        public string Email { get; set; }
        public List<RoleModel> Roles { get; set; }
    }
}
