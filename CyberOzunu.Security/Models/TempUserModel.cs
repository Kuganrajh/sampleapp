﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Models
{
    public class TempUserModel
    {
        public object User { get; set; }
        public string Password { get; set; }
        public Guid RoleId { get; set; }
        public Guid OriginId { get; set; }
    }
}
