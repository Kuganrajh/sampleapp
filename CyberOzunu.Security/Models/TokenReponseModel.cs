﻿using CyberOzunu.Security.Config;
using System;
using System.Text.Json.Serialization;

namespace CyberOzunu.Security.Models
{
    public class TokenReponseModel
    {
        [JsonIgnore]
        public Guid UserId { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public TokenType TokenType { get;  set; }
        public UserType UserType { get; set; }
        public bool IsNewUser { get; set; }

        //TODO: Need to be removed
        public string OTP { get; set; }
    }
}
