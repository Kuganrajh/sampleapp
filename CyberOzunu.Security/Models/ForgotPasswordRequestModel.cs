﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Models
{
    public class ForgotPasswordRequestModel
    {
        public string NewPassword { get; set; }
        public string Token { get; set; }
    }
}
