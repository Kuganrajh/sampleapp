﻿using CyberOzunu.Security.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.Security.Persistence
{
    public class IdentityDbContextFactory : DesignTimeDbContextFactoryBase<ApplicationDbContext>
    {
       
        protected override ApplicationDbContext CreateNewInstance(DbContextOptions<ApplicationDbContext> options)
        {
            return new ApplicationDbContext(options);
        }

    }
}
