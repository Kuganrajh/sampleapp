﻿using Cyberozunu.OnlineTaxApplication.DAL.Base;
using Cyberozunu.OnlineTaxApplication.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.DAL.Interface
{
    public interface ICustomerRepository : IBaseRepository<Customer>
    {
    }
}
