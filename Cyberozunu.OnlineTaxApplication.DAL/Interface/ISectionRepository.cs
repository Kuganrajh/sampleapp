﻿using Cyberozunu.OnlineTaxApplication.DAL.Base;
using Cyberozunu.OnlineTaxApplication.Entity.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.DAL
{
    public interface ISectionRepository : IBaseRepository<Section>
    {
    }
}
