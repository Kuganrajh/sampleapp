﻿using Cyberozunu.OnlineTaxApplication.DAL.Context;
using Cyberozunu.OnlineTaxApplication.Entity.Common;
using CyberOzunu.Core.EFCore.Model;
using CyberOzunu.LinqFunction.Generics;
using CyberOzunu.LinqFunction.Pagination;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.DAL.Base
{
    public class BaseRepository<T> where T : BaseEntity
    {
        protected readonly DBContext _dbContext;

        public BaseRepository(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task<T> Add(T entity)
        {
            var result = await _dbContext.Set<T>().AddAsync(entity);
            return result.Entity;
        }

        public virtual async Task<List<T>> AddRange(List<T> entity)
        {
            await _dbContext.Set<T>().AddRangeAsync(entity);
            return entity;
        }

        public virtual T Update(T entity)
        {
            entity.ModifiedOn = DateTime.UtcNow;
            var result = _dbContext.Set<T>().Update(entity).Entity;
            return result;
        }

        public virtual List<T> UpdateRange(List<T> entity)
        {
            _dbContext.Set<T>().UpdateRange(entity);
            return entity;
        }

        public virtual async Task<ResponseStatus> Delete(Guid id)
        {
            ResponseStatus responseStatus = new ResponseStatus();
            var existingData = await Get(id);
            if (existingData == null)
            {
                responseStatus.Status = false;
                responseStatus.Data = "No data found";
            }
            _dbContext.Set<T>().Remove(existingData);
            return responseStatus;
        }


        public virtual async Task<T> Get(Guid id, List<string> includes = null)
        {
            var data = (from i in _dbContext.Set<T>() where i.Id == id select new { i }).AsNoTracking();
            var type = typeof(T);
            var entityResult = new AddInclude().Include<T>(data, "i", includes);
            var result = entityResult.FirstOrDefault();
            return result;
        }

        public virtual async Task<CollectionResult<T>> GetAll(FilterParameter filterParameter, IQueryable<T> query = null)
        {
            filterParameter.SortBy = string.IsNullOrEmpty(filterParameter.SortBy) ? nameof(BaseEntity.CreatedOn) : filterParameter.SortBy;
            CollectionResult<T> collectionResult = new CollectionResult<T>();
            var filterExpression = new Filter<T>();

            if (query == null)
            {
                query = _dbContext.Set<T>().AsQueryable().AsNoTracking();
            }
            if (filterParameter != null && filterParameter.Filters != null)
            {
                foreach (var filter in filterParameter.Filters)
                {
                    filterExpression.By(filter.Property, filter.Operator, filter.value, null, filterParameter.FilterStatementConnector);
                }
            }

            query = query.Where(filterExpression).AsQueryable();
            if (filterParameter != null && filterParameter.PageNumber.HasValue && filterParameter.PageSize.HasValue)
            {
                var paginatedResult = await PaginationService.Paginate<T>(query, filterParameter.PageNumber, filterParameter.PageSize, filterParameter.SortBy, filterParameter.IsDecending);
                collectionResult.Data = await Task.Run(() => paginatedResult.Results.ToList());
                collectionResult.Count = paginatedResult.TotalItems;
            }
            else
            {
                collectionResult.Data = await Task.Run(() => query.AsNoTracking().ToList());
                collectionResult.Count = collectionResult.Data.Count;
            }
            return collectionResult;
        }

        public virtual async Task<List<T>> GetByIds(List<Guid> ids)
        {
            var data = await _dbContext.Set<T>().Where(p => ids.Contains(p.Id)).ToListAsync();
            return data;
        }

        public virtual async Task<List<ETYPE>> GetExternalData<ETYPE>(FilterParameter filterParameter) where ETYPE : BaseEntity
        {
            var filterExpression = new Filter<ETYPE>();
            var query = _dbContext.Set<ETYPE>().AsQueryable().AsNoTracking();
            if (filterParameter != null && filterParameter.Filters != null)
            {
                foreach (var filter in filterParameter.Filters)
                {
                    filterExpression.By(filter.Property, filter.Operator, filter.value, null, filterParameter.FilterStatementConnector);
                }
            }
            query = query.Where(filterExpression).AsQueryable();
            var result = await Task.Run(() => query.AsNoTracking().ToList());
            return result;
        }
        public virtual async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

    }
}
