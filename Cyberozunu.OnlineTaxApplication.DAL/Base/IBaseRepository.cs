﻿using Cyberozunu.OnlineTaxApplication.Entity.Common;
using CyberOzunu.Core.EFCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyberozunu.OnlineTaxApplication.DAL.Base
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        Task<T> Add(T entity);
        Task<List<T>> AddRange(List<T> entity);
        T Update(T entity);
        Task<T> Get(Guid id, List<string> includes = null);
        Task<ResponseStatus> Delete(Guid id);
        Task<CollectionResult<T>> GetAll(FilterParameter filterParameter, IQueryable<T> query = null);
        Task<List<T>> GetByIds(List<Guid> ids);
        Task SaveChangesAsync();
    }
}
