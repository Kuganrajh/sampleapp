﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Threading;

namespace Cyberozunu.OnlineTaxApplication.DAL.Context
{
    public partial class DBContext
    {
        //protected List<AuditEntry> OnBeforeSaveChanges()
        //{
        //    ChangeTracker.DetectChanges();
        //    var auditEntries = new List<AuditEntry>();
        //    foreach (var entry in ChangeTracker.Entries())
        //    {
        //        if (!(entry.Entity is  IAuditable) || entry.State == EntityState.Detached || entry.State == EntityState.Unchanged)
        //            continue;

        //        var auditEntry = new AuditEntry(entry);
        //        auditEntry.TableName = entry.Metadata.GetTableName(); 
        //        auditEntries.Add(auditEntry);

        //        foreach (var property in entry.Properties)
        //        {
                   
        //            if (property.IsTemporary)
        //            {
        //                auditEntry.TemporaryProperties.Add(property);
        //                continue;
        //            }

        //            string propertyName = property.Metadata.Name;
        //            if (property.Metadata.IsPrimaryKey())
        //            {
        //                auditEntry.KeyValue = Guid.Parse(property.CurrentValue.ToString());
        //                continue;
        //            }

        //            switch (entry.State)
        //            {
        //                case EntityState.Added:
        //                    auditEntry.NewValues[propertyName] = property.CurrentValue;
        //                    break;

        //                case EntityState.Deleted:
        //                    auditEntry.OldValues[propertyName] = property.OriginalValue;
        //                    break;

        //                case EntityState.Modified:
        //                    if (property.IsModified)
        //                    {
        //                        auditEntry.OldValues[propertyName] = property.OriginalValue;
        //                        auditEntry.NewValues[propertyName] = property.CurrentValue;
        //                    }
        //                    break;
        //            }
        //        }
        //    }

        //    // Save audit entities that have all the modifications
        //    foreach (var auditEntry in auditEntries.Where(_ => !_.HasTemporaryProperties))
        //    {
        //        AuditHistories.Add(auditEntry.ToAudit());
        //    }

        //    // keep a list of entries where the value of some properties are unknown at this step
        //    return auditEntries.Where(_ => !_.HasTemporaryProperties).ToList();
        //}


        //protected Task OnAfterSaveChanges(List<AuditEntry> auditEntries)
        //{
        //    if (auditEntries == null || auditEntries.Count == 0)
        //        return Task.CompletedTask;

        //    foreach (var auditEntry in auditEntries)
        //    {
        //        // Get the final value of the temporary properties
        //        foreach (var prop in auditEntry.TemporaryProperties)
        //        {
        //            if (prop.Metadata.IsPrimaryKey())
        //            {
        //                auditEntry.KeyValue = Guid.Parse(prop.CurrentValue.ToString());
        //            }
        //            else
        //            {
        //                auditEntry.NewValues[prop.Metadata.Name] = prop.CurrentValue;
        //            }
        //        }

        //        // Save the Audit entry
        //        AuditHistories.Add(auditEntry.ToAudit());
        //    }

        //    return SaveChangesAsync();
        //}

        private ClaimsPrincipal GetCurrentPrincipal()
        {
            return Thread.CurrentPrincipal as ClaimsPrincipal;
        }
    }
}
