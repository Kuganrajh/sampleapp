﻿using CyberOzunu.Core.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.DAL.Context
{
    public class DBContextFactory : IDesignTimeDbContextFactory<DBContext>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        //public DBContextFactory(IHttpContextAccessor httpContextAccessor)
        //{
        //    _httpContextAccessor = httpContextAccessor;
        //}
        public DBContext CreateDbContext(string[] args)
        {
            var connectionString = AppSetting.GetConnectionString("dbConfig");
            var optionalBuilder = new DbContextOptionsBuilder<DBContext>();
            optionalBuilder.UseSqlServer(connectionString);
            return new DBContext(optionalBuilder.Options, _httpContextAccessor);
        }
    }
}
