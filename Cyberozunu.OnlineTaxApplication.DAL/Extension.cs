﻿using Cyberozunu.OnlineTaxApplication.DAL.Base;
using Cyberozunu.OnlineTaxApplication.DAL.Context;
using Cyberozunu.OnlineTaxApplication.DAL.Implementation;
using Cyberozunu.OnlineTaxApplication.DAL.Interface;
using CyberOzunu.Core.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.DAL
{
    public static class Extensions
    {
        public static void AddDALExtension(this IServiceCollection services)
        {

            var connectionString = AppSetting.GetConnectionString("dbConfig");
            services.AddDbContext<DBContext>(options =>
            {
                options.UseSqlServer(connectionString, sqlServerOptionsAction: sqloption => { sqloption.EnableRetryOnFailure(); });

            });
            services.AddScoped<DBContext>();
            services.AddScoped(typeof(BaseRepository<>), typeof(BaseRepository<>));

            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IPackageRepository, PackageRepository>();
            services.AddScoped<ISectionRepository, SectionRepository>();
        }

        public static void UpdateDatabase(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<DBContext>())
                {
                    context.Database.Migrate();
                }
            }
        }
    }
}
