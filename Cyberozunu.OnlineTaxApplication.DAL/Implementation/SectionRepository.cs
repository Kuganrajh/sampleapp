﻿using Cyberozunu.OnlineTaxApplication.DAL.Base;
using Cyberozunu.OnlineTaxApplication.DAL.Context;
using Cyberozunu.OnlineTaxApplication.Entity.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.DAL.Implementation
{
    internal class SectionRepository : BaseRepository<Section>, ISectionRepository
    {
        public SectionRepository(DBContext dbContext) : base(dbContext)
        {
        }
    }
}
