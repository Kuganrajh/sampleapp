﻿using Cyberozunu.OnlineTaxApplication.DAL.Base;
using Cyberozunu.OnlineTaxApplication.DAL.Context;
using Cyberozunu.OnlineTaxApplication.DAL.Interface;
using Cyberozunu.OnlineTaxApplication.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cyberozunu.OnlineTaxApplication.DAL.Implementation
{
    internal class PackageRepository : BaseRepository<Package>, IPackageRepository
    {
        public PackageRepository(DBContext dbContext):base(dbContext)
        {

        }
    }
}
