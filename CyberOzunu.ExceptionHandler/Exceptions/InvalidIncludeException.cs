﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.ExceptionHandler.Exceptions
{
    public class InvalidIncludeException : Exception
    {
        public InvalidIncludeException()
        { }

        public InvalidIncludeException(string propery, Type type)
            : base(@"Invalid Include field '" + propery + "' in " + type.Name)
        { }


        public InvalidIncludeException(string propery, Exception innerException, Type type)
            : base(@"Invalid Include field '" + propery + "' in " + type.Name, innerException)
        { }
    }
}
