﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.ExceptionHandler.Exceptions
{
    public class AzureBlobException : Exception
    {
        public AzureBlobException()
        { }

        public AzureBlobException(string message)
            : base(message)
        { }


        public AzureBlobException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
