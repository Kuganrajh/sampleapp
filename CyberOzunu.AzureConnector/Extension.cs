﻿using CyberOzunu.AzureConnector.BlobService;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.AzureConnector
{
    public static class Extension
    {
        public static void AddAzureConnector(this IServiceCollection services)
        {
            services.AddScoped<IBlobHandler, BlobHandler>();
        }
    }
}
