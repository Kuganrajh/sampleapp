﻿using CyberOzunu.AzureConnector.Model.Blob;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CyberOzunu.AzureConnector.BlobService
{
    public interface IBlobHandler 
    {
        Task<AzureBlobResponseModel> UploadTempImageAsync(IFormFile file, string name, string folder = null);
        Task<AzureBlobResponseModel> MoveImageToRelevantFolder(string existingpath, string subPath, string name);
    }
}
