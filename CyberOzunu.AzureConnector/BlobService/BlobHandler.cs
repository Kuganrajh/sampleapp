﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CyberOzunu.AzureConnector.Enum;
using CyberOzunu.AzureConnector.Model.Blob;
using CyberOzunu.ExceptionHandler.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace CyberOzunu.AzureConnector.BlobService
{
    internal class BlobHandler : IBlobHandler
    {
        private readonly IOptions<BlobConnection> _config;

        public BlobHandler(IOptions<BlobConnection> Config)
        {
            this._config = Config;
        }
        public async Task<AzureBlobResponseModel> UploadTempImageAsync(IFormFile file,string name,string folder=null)
        {
            try
            {
                AzureBlobResponseModel azureBlobResponseModel = new AzureBlobResponseModel();
                if (CloudStorageAccount.TryParse(_config.Value.Connection, out CloudStorageAccount storageAccount))
                {
                    string path = (!string.IsNullOrEmpty(folder) ? folder+"/" : "Temp/") + name.ToLower();
                    CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
                    CloudBlobContainer container = cloudBlobClient.GetContainerReference(_config.Value.Container);

                    CloudBlob blob = container.GetBlobReference(path);
                    if (await blob.ExistsAsync())
                    {
                        await blob.DeleteAsync();
                    }
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(path);
                    await blockBlob.UploadFromStreamAsync(file.OpenReadStream());
                    azureBlobResponseModel.ImageURL = blockBlob.Uri.AbsoluteUri;
                    return azureBlobResponseModel;


                }
                else
                {
                    throw new AzureBlobException("No storage with provided credentials");
                }
            }
            catch (Exception ex)
            {
                throw new AzureBlobException(ex.Message, ex);
            }
        }


        public async Task<AzureBlobResponseModel> MoveImageToRelevantFolder(string existingpath,string subPath, string name)
        {
            try
            {
                AzureBlobResponseModel azureBlobResponseModel = new AzureBlobResponseModel();
                string path = subPath + "/" + name;
                var imagePaths = existingpath.Split("/");
                var imageName = imagePaths[imagePaths.Length - 2] + "/" + imagePaths[imagePaths.Length - 1];
                if (CloudStorageAccount.TryParse(_config.Value.Connection, out CloudStorageAccount storageAccount))
                {
                    var myClient = storageAccount.CreateCloudBlobClient();
                    var container = myClient.GetContainerReference(_config.Value.Container);

                    var blockBlob = container.GetBlockBlobReference(imageName);
                    var copyBlockBlob = container.GetBlockBlobReference(path);
                    if (await copyBlockBlob.ExistsAsync())
                    {
                        await copyBlockBlob.DeleteAsync();
                    }

                    await copyBlockBlob.StartCopyAsync(blockBlob);
                    azureBlobResponseModel.ImageURL = copyBlockBlob.StorageUri.PrimaryUri.ToString();
                }
                return azureBlobResponseModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
