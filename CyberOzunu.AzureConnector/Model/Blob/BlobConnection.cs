﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.AzureConnector.Model.Blob
{
    public class BlobConnection
    {
        public string Connection { get; set; }
        public string Container { get; set; }
    }
}
