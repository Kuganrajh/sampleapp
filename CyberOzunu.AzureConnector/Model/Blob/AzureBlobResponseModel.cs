﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CyberOzunu.AzureConnector.Model.Blob
{
    public class AzureBlobResponseModel
    {
        public string ImageURL { get; set; }
        public string Thumpnail { get; set; }
    }
}
